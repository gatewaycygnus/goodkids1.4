//
//  childselectionViewController.swift
//  GoodKid
//
//  Created by Kumar Muthusamy on 11/10/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class childselectionViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    

    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var tableviewselctn: UITableView!
    var childRegisterList = [String]()
     var childgenderList = [String]()
    var childKeyList = [String]()
    var childimgeList = [String]()
    var boxView = UIView()
    var textLabel = UILabel()
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            self.lodingView(temp: true)
        }
        //    self.lodingView(temp: true)
        
        ref = Database.database().reference()
        let userID1 = Auth.auth().currentUser?.uid
        UserDefaults.standard.set(userID1, forKey: "parentid")
        
        ref.child("Children").child(userID1!).observe(.value, with: {
            snapshot in
            self.childRegisterList.removeAll()
            self.childKeyList.removeAll()
            self.childimgeList.removeAll()
            self.childgenderList.removeAll()
            for group in snapshot.children {
                self.ref.child("Children").child(userID1!).child((group as AnyObject).key).observeSingleEvent(of: .value, with: { (snapshot) in
                    print("name---value---->555 )",snapshot.key)
                    
                    self.childKeyList.append(snapshot.key)
                    let value = snapshot.value as? NSDictionary
                    let profileurl = value?["profileURL"] as? String ?? ""
                    let name = value?["Name"] as? String ?? ""
                    let gender = value?["Gender"] as? String ?? ""
                    self.childRegisterList.append(name)
                    self.childgenderList.append(gender)
                    self.tableviewselctn.reloadData()
                    //self.tableView.reloadData()
                    print("name--->1111",name)
                    print("name--->2222",self.childRegisterList)
                    self.childimgeList.append(profileurl)
                    print("ggg",self.childRegisterList.count)
                    print("hari",self.childimgeList.count)
                    DispatchQueue.main.async {
                        self.tableviewselctn.reloadData()
                        print("name",self.childRegisterList.count)
                        print("image",self.childimgeList.count)
                    }
                    
                })
            }
        })
        
        
    }
    
    
    
    func lodingView(temp:Bool){
        // You only need to adjust this frame to move it anywhere you want
        
        if(temp)
        {
            boxView = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50))
            textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        }else
        {
            boxView = UIView(frame: CGRect(x: view.frame.midX - 185, y: view.frame.midY - 25, width: 370, height:50))
            textLabel = UILabel(frame: CGRect(x: 36, y: 0, width: 370, height:50))
        }
        
        boxView.backgroundColor = UIColor.white
        boxView.alpha = 0.8
        boxView.layer.cornerRadius = 10
        
        //Here the spinnier is initialized
        var activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 45, height: 50)
        activityView.startAnimating()
        
        //        textLabel = UILabel(frame: CGRect(x: 36, y: 0, width: 370, height:50))
        textLabel.textColor = UIColor.gray
        //        textLabel.font = UIFont.systemFont(ofSize: 10)
        textLabel.text = "Loading"
        //        textLabel.sizeToFit()
        textLabel.adjustsFontSizeToFitWidth = true
        
        boxView.addSubview(activityView)
        
        boxView.addSubview(textLabel)
        self.view.addSubview(boxView)
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
        //  return UITableViewAutomaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print("lllll",childRegisterList.count)
        
        return childimgeList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! childselectionTableViewCell
        tableView.layer.cornerRadius = 25.0
        print("llllldddd",self.childRegisterList[indexPath.row])
        print("llllldddd2",self.childimgeList[indexPath.row])
//        DispatchQueue.main.async {
            if(self.childRegisterList[indexPath.row] != nil){
                
                if self.childgenderList[indexPath.row].elementsEqual("Male"){
                    cell.childimage.image = #imageLiteral(resourceName: "icon_Little_Boy")
                } else {
                    cell.childimage.image = #imageLiteral(resourceName: "icon_little_girl")
                }
                
//                if(profile.isEmpty){
//                    print("nothing")
//                }else{
//                    let url = URL(string: profile)
//                    UserDefaults.standard.set(url, forKey: "childprofileurl")
//
//                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
//                    cell.childimage.image = UIImage(data: data!)
//
//                }
                cell.childname.text = self.childRegisterList[indexPath.row]
            }
//        }
        
        
        DispatchQueue.main.async {
            self.boxView.removeFromSuperview()
        }
        
        return cell
        
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedItem = childRegisterList[indexPath.row]
        let selecteditemimage = childimgeList[indexPath.row]
        print("You tapped cell number ",selectedItem)
        
        
        let res:ParentManageViewController = self.storyboard?.instantiateViewController(withIdentifier: "ParentManageViewController") as!ParentManageViewController
        
        res.strmail = childRegisterList[indexPath.row]
        UserDefaults.standard.set(childRegisterList[indexPath.row], forKey: "childname")
        
        UserDefaults.standard.set(childKeyList[indexPath.row], forKey: "childkey")
        
        res.url = childimgeList[indexPath.row]
        
        UserDefaults.standard.set(childimgeList[indexPath.row], forKey: "url")
        
        
        let navController = UINavigationController(rootViewController: res) // Creating a navigation controller with VC1 at the root of the navigation stack.
        self.present(navController, animated:true, completion: nil)
        

        print("---parentManageView---ManageChild--- childName",UserDefaults.standard.string(forKey: "childname"))
        
    }
    
    
    @IBAction func okbutton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
