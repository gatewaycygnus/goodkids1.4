//
//  ChildUnRegTableViewCell.swift
//  GoodKid
//
//  Created by cyg on 10/5/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit

class ChildUnRegTableViewCell: UITableViewCell {

 
    
    @IBOutlet weak var childImg: UIImageView!
    
    @IBOutlet weak var childName: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        self.childImg.layer.cornerRadius = self.childImg.frame.size.width / 2;
        self.childImg.clipsToBounds = true;
        self.childImg.layer.borderWidth = 6.0
        self.childImg.layer.borderColor = UIColor.white.cgColor
        // Do any additional setup after loading the view.
        self.childImg.layer.shadowOpacity = 0.5;
        self.childImg.layer.shadowRadius = 2.0;
           
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
