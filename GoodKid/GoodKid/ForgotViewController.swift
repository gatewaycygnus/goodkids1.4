//
//  ForgotViewController.swift
//  GoodKid
//
//  Created by Kumar Muthusamy on 9/24/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class ForgotViewController: UIViewController, UITextFieldDelegate{

    @IBOutlet weak var email: UITextField!
    var strmail:String! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        email.layer.borderColor = UIColor.white.cgColor
        email.layer.borderWidth = 2
        email.tintColor = UIColor.white
        

email.text = strmail
        self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "pic3"),for: .default)
        email.delegate = self 
        // Do any additional setup after loading the view.
    }
    func textFieldShouldReturn(_ textfield: UITextField) -> Bool {
       
        email.resignFirstResponder()
        
        return true;
    }

    @IBOutlet weak var resetpass: UIButton!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func resetbtn(_ sender: Any) {
        
        
        let resetEmail = email.text
        Auth.auth().sendPasswordReset(withEmail: resetEmail!, completion: { (error) in
            if error != nil{
                let resetFailedAlert = UIAlertController(title: "Reset Failed", message: "Error: \(String(describing: error?.localizedDescription))", preferredStyle: .alert)
                resetFailedAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(resetFailedAlert, animated: true, completion: nil)
            }else {
                let resetEmailSentAlert = UIAlertController(title: "Reset email sent successfully", message: "Check your email", preferredStyle: .alert)
                
                resetEmailSentAlert.addAction(UIAlertAction(title: "OK", style: .default, handler:{(UIAlertAction) in
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController")
                    self.navigationController?.pushViewController(vc!, animated: true)
                    
                }))
                
//
                self.present(resetEmailSentAlert, animated: true, completion: nil)
            }
        })
    
        
    }

    @IBAction func btnback(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewController") as? ViewController
        self.navigationController?.pushViewController(vc!, animated: true)
        
        
    }
}
