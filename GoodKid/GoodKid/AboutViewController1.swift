//
//  AboutViewController1.swift
//  GoodKid
//
//  Created by Kumar Muthusamy on 12/2/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit
import WebKit

class AboutViewController1: UIViewController {

//    var webView: WKWebView!
//
//    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
//
//    }
    
    var boxView = UIView()
    var textLabel = UILabel()
    
    @IBOutlet weak var webview: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        navigationController?.navigationBar.barTintColor = UIColor.orange
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        
        DispatchQueue.main.async {
            self.lodingView(temp: true)
        }
    
        
//        self.setupWebView()
//        self.view.addSubview(self.webView)
//
        // Do any additional setup after loading the view.
        
        let htmlpath = Bundle.main.path(forResource: "about",ofType: "html")
        let url = URL(fileURLWithPath: htmlpath!)
        let request = URLRequest(url: url)
        webview.load(request)
        
        DispatchQueue.main.async {
            self.boxView.removeFromSuperview()
        }
        
    }
    

//    private func setupWebView() {
//
//        let contentController = WKUserContentController()
//        let userScript = WKUserScript(
//            source: "mobileHeader()",
//            injectionTime: WKUserScriptInjectionTime.atDocumentEnd,
//            forMainFrameOnly: true
//        )
//        contentController.addUserScript(userScript)
//        //        contentController.add(self, name: "loginAction")
//
//        let config = WKWebViewConfiguration()
//        config.userContentController = contentController
//        self.webView = WKWebView(frame: self.view.bounds, configuration: config)
//        print("---mobileHeader---")
//    }
//
    
    func lodingView(temp:Bool){
        // You only need to adjust this frame to move it anywhere you want
        
        if(temp)
        {
            boxView = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50))
            textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        }else
        {
            boxView = UIView(frame: CGRect(x: view.frame.midX - 185, y: view.frame.midY - 25, width: 370, height:50))
            textLabel = UILabel(frame: CGRect(x: 36, y: 0, width: 370, height:50))
        }
        
        boxView.backgroundColor = UIColor.white
        boxView.alpha = 0.8
        boxView.layer.cornerRadius = 10
        
        //Here the spinnier is initialized
        var activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 45, height: 50)
        activityView.startAnimating()
        //        textLabel = UILabel(frame: CGRect(x: 36, y: 0, width: 370, height:50))
        textLabel.textColor = UIColor.gray
        //        textLabel.font = UIFont.systemFont(ofSize: 10)
        textLabel.text = "Loading"
        //        textLabel.sizeToFit()
        textLabel.adjustsFontSizeToFitWidth = true
        boxView.addSubview(activityView)
        boxView.addSubview(textLabel)
        self.view.addSubview(boxView)
        
    }
    
    
    

}
