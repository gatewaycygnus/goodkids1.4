//
//  ViewController.swift
//  GoodKid
//
//  Created by Kumar Muthusamy on 9/14/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class ViewController: UIViewController,UITextFieldDelegate, URLSessionDelegate {
    
    
    @IBOutlet weak var view1: UIView!
    var boxView = UIView()
    var textLabel = UILabel()
    
   @IBOutlet weak var login: UIButton!
    @IBOutlet weak var scrooll: UIScrollView!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        //  self.lodingView(temp: true)
        self.httpGet(request: URLRequest( url: URL(string: "https://good-kidz.com/servercheck")!))
//        view1.backgroundColor = UIColor(patternImage: UIImage(named:"pic3")!)
//        if Auth.auth().currentUser != nil {
//            let userID1 = Auth.auth().currentUser?.uid
//            UserDefaults.standard.set(userID1, forKey: "parentid")
//            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ManageChildStackViewController") as? ManageChildStackViewController
//       self.dismiss(animated: true, completion:nil)
//        } else {
       print("******************",UIDevice.current.identifierForVendor?.uuidString)
            
            login.isMultipleTouchEnabled = false
            email.layer.borderWidth = 2
            email.tintColor = UIColor.white
            email.layer.borderColor = UIColor.white.cgColor
            password.layer.borderColor = UIColor.white.cgColor
            password.layer.borderWidth = 2
            password.tintColor = UIColor.white
            login.backgroundColor = UIColor.white
            login.layer.cornerRadius = 20
            login.isEnabled = true;
            email.delegate = self
            password.delegate = self
            
            
            //        scrooll.layoutIfNeeded()
            //        scrooll.isScrollEnabled = true
            //        scrooll.contentSize = CGSize(width: self.view.frame.width, height: scrooll.frame.size.height)
            var image = UIImage(named: "image1") as! UIImage
    self.navigationController?.navigationBar.setBackgroundImage(image,for: .default)
        login.addTarget(self, action: #selector(loginaccess), for: .touchUpInside)
        
//        }
        
    }
    
    
    @objc func loginaccess(){
        print("Subscription is applied")
        
        login.isMultipleTouchEnabled = false
        var Email = email.text
        var Pass = password.text
        print("---------------->email------>",Email)
        print("---------------->email------>",Pass)
        if self.email.text == "" || self.password.text == "" {
            let alertController = UIAlertController(title: "Error", message: "Please enter an email and password.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            print("jjjjjjjjj")
            Auth.auth().signIn(withEmail: self.email.text!, password: self.password.text!) { (user, error) in
                if error == nil {
                    //Print into the console if successfully logged in
                    print("You have successfully logged in")
                    //self.lodingView(temp: true)
                    DispatchQueue.main.async {
                        self.lodingView(temp: true)
                    }
//   let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ManageChildStackViewController")
                    
//  self.navigationController?.pushViewController(vc!, animated: true)
                    
    guard let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ManageChildStackViewController") else { return }
    let navController = UINavigationController(rootViewController: myVC)

    self.navigationController?.present(navController, animated: true, completion: nil)
                    
            self.email.text = nil
            self.password.text = nil
                }
                    
        else {
            
                    
        let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)
                    
                }
            }
        }
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        print("dddd")
        if(email.text!.isEmpty) || (password.text!.isEmpty) || (password.text!.count < 6){
            print("jjjj")
            
            login.layer.borderColor =  #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            login.layer.borderWidth = 2
            login.setTitleColor(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), for: UIControlState.normal)
        }
        else
        {
            login.layer.borderColor =  #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            login.layer.borderWidth = 2
            login.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: UIControlState.normal)

        }
        
    }
    
    
    
    //    func textFieldDidBeginEditing(_ textField: UITextField){
    //        if (email.text!.count != 0) && !(password.text!.count != 0){
    //            login.layer.borderColor =  #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
    //            login.layer.borderWidth = 2
    //            print("hhhhh")
    //
    //            login.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: UIControlState.normal)
    //        }
    //
    //        else{
    //            print("pothe",email.text?.count)
    //
    //        }
    //            // login.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.3218928426, blue: 0.8372200415, alpha: 0.5118965955)
    //            //            login.layer.borderColor =  #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
    //            //            login.layer.borderWidth = 5
    //
    //
    //        }
    //
    
    
//    @IBAction func Login(_ sender: Any) {
//
//        login.isMultipleTouchEnabled = false
//        var Email = email.text
//        var Pass = password.text
//        print("---------------->email------>",Email)
//        print("---------------->email------>",Pass)
//        if self.email.text == "" || self.password.text == "" {
//            let alertController = UIAlertController(title: "Error", message: "Please enter an email and password.", preferredStyle: .alert)
//            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
//            alertController.addAction(defaultAction)
//            self.present(alertController, animated: true, completion: nil)
//        }
//        else {
//            print("jjjjjjjjj")
//            Auth.auth().signIn(withEmail: self.email.text!, password: self.password.text!) { (user, error) in
//                    if error == nil {
//                    //Print into the console if successfully logged in
//                    print("You have successfully logged in")
//                    //self.lodingView(temp: true)
//                    DispatchQueue.main.async {
//                        self.lodingView(temp: true)
//                    }
//                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ManageChildStackViewController") as? ManageChildStackViewController
//                    self.navigationController?.pushViewController(vc!, animated: true)
//                    self.email.text = nil
//                    self.password.text = nil
//                }
//
//      else {
//      let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
//      let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
//                    alertController.addAction(defaultAction)
//                    self.present(alertController, animated: true, completion: nil)
//
//                }
//            }
//        }
//
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func Register(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RegisterViewController1") as? RegisterViewController
        self.navigationController?.pushViewController(vc!, animated: true)
        
        
        
    }
    
    func textFieldShouldReturn(_ textfield: UITextField) -> Bool {
        
        textfield.resignFirstResponder()
        return true;
    }
    
    
    
    func lodingView(temp:Bool){
        // You only need to adjust this frame to move it anywhere you want
        
        if(temp)
        {
            //            boxView = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50))
            //            textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
            boxView = UIView(frame: CGRect(x: view.frame.midX - 110, y: view.frame.midY - 25, width: 220, height: 50))
            textLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 220, height: 50))
        }else
        {
            boxView = UIView(frame: CGRect(x: view.frame.midX - 185, y: view.frame.midY - 25, width: 370, height:50))
            textLabel = UILabel(frame: CGRect(x: 36, y: 0, width: 370, height:50))
        }
        
        boxView.backgroundColor = UIColor.white
        boxView.alpha = 0.8
        boxView.layer.cornerRadius = 10
        
        //Here the spinnier is initialized
        var activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 45, height: 50)
        activityView.startAnimating()
        
        //        textLabel = UILabel(frame: CGRect(x: 36, y: 0, width: 370, height:50))
        textLabel.textColor = UIColor.gray
        //        textLabel.font = UIFont.systemFont(ofSize: 10)
        textLabel.text = "Waiting for server..."
        //        textLabel.sizeToFit()
        textLabel.adjustsFontSizeToFitWidth = true
        //        textLabel.textAlignment = .center
        
        boxView.addSubview(activityView)
        
        boxView.addSubview(textLabel)
        self.view.addSubview(boxView)
        
    }
    func httpGet(request: URLRequest) {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 30
        
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue:OperationQueue.main)
        let task = session.dataTask(with: request){
            (data, response, error) -> Void in
            if error == nil {
                var result = NSString(data: data!, encoding:
                    String.Encoding.ascii.rawValue)!
                NSLog("result %@", result)
                
                if(result != "I am Fine")
                {
                    let alert = UIAlertController(title: "Alert", message: "Server down, please try again later, the changes made will not be affected", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }else
                {
                    DispatchQueue.main.async {
                        self.view.viewWithTag(100)?.removeFromSuperview()
                        self.boxView.removeFromSuperview()
                    }
                }
            }
            else{
                
                let alert = UIAlertController(title: "Alert", message: error?.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                self.boxView.removeFromSuperview()
                
            }
        }
        
        task.resume()
    }
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!) )
    }
    
    @IBAction func Forgotpassword(_ sender: Any) {
        if self.email.text == ""{
            let alertController = UIAlertController(title: "Error", message: "Please enter an email ", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else{
            
            let databaseReff = Database.database().reference().child("Users")
            
            databaseReff.queryOrdered(byChild: "email").queryEqual(toValue: self.email.text!).observe(.value, with: { snapshot in
                
                //User email exist
                if snapshot.exists(){
                    
                    
                    print("success")
                    let res:ForgotViewController = self.storyboard?.instantiateViewController(withIdentifier: "ForgotViewController1") as!ForgotViewController
                    
                    res.strmail = self.email.text
                    
                    self.navigationController?.pushViewController(res, animated: true)
                    
                }
                else{
                    //email does not [email id available]
                    
                    let alertController = UIAlertController(title: "Error", message: "Please enter a registerd email ", preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                
            })
            
        }
        
    }
    
}
extension UIView {
    func roundCorners1(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

