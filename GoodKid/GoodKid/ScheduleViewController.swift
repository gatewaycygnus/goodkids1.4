//
//  ScheduleViewController.swift
//  GoodKid
//
//  Created by Kumar Muthusamy on 10/25/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit
import Firebase
//import CellAnimator
import FirebaseAuth
import XLPagerTabStrip

class ScheduleViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,IndicatorInfoProvider
{
    
    @IBOutlet weak var butonview: UIView!
    
    
    @IBOutlet weak var scheduleadd: UIButton!
    
    
    @IBOutlet weak var sheduldview: UIView!
    @IBOutlet weak var sheduletableview: UITableView!
    var subscription = ""
    var  blockcount = 0
    var childNumber:String = ""
    var ref: DatabaseReference!
    var scheduledict:NSDictionary = [String: Any]() as NSDictionary
    var category :NSDictionary = [String: Any]() as NSDictionary
    var newname = " "
    var counter =  1
    var time = " "
    var switch1 = true
    var ismonday = true
    var istusdy = true
    var iswednsdy = true
    var isthursdy = true
    var isfridy = true
    var isstrdy = true
    var issundy = true
    var childname = " "
    var childkey = " "
    
    var weekdayarray = ["Monday","Tuesday","Wednesday","Thursday","Friday"]
    var weekendarray = ["Saturday","Sunday"]
    var typeshedulearry = [String]()
    var catgryarray = [String]()
    var typeshedulearry2 = [String]()
    let timePicker = UIDatePicker()
    var wkdystartTime = " "
    var wkdyendTime = " "
    var wkendstartTime = " "
    var wkendendTime = " "
    

 
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
      //  self.scheduleadd.layer.cornerRadius = self.scheduleadd.frame.size.width / 3;
//    self.scheduleadd.layer.cornerRadius = 10;
//        
//        
//        self.scheduleadd.clipsToBounds = true;
//        self.scheduleadd.layer.borderWidth = 2.0
//        self.scheduleadd.layer.borderColor = UIColor.white.cgColor
//        
       ref = Database.database().reference()
        let userID1 = Auth.auth().currentUser?.uid
        ref.child("Users").child(userID1!).observe(DataEventType.value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            print("values",value)
            
            self.subscription = value?["subscription"] as? String ?? ""
            print("subscription", self.subscription)
            UserDefaults.standard.set(self.subscription, forKey: "subscription")
           self.sheduletableview.reloadData()
            
        })
        
        
        
        
        
        
        
        
        
        childname = UserDefaults.standard.string(forKey: "childname")!
        childkey = UserDefaults.standard.string(forKey: "childkey")!
        ref = Database.database().reference()
        //let userID1 = Auth.auth().currentUser?.uid
        
        ref.child("Children").child(userID1!).child(childkey).observe(DataEventType.value, with: { (snapshot) in
            
            // Get user value
            self.typeshedulearry.removeAll()
            
            let value = snapshot.value as? NSDictionary
            print("my value",value)
            if(value?["Master Schedule"] != nil)
            {
            let dict = value?["Master Schedule"] as? NSDictionary
            print("sheduledict",dict)
            self.scheduledict = dict!
            
            for ele in dict!{
                var type = ele.key as! String
                print("ele",ele.key)
                self.typeshedulearry.append(ele.key as! String)
                DispatchQueue.main.async {
                    self.sheduletableview.reloadData()
                    print("name",self.typeshedulearry.count)
                }
            }
        }
        })
        
        //        { (error) in
        //            print(error.localizedDescription)
        //        }
        
        
        self.sheduletableview.layer.cornerRadius = 10.0
        self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "pic3"),for: .default)
        
        // Do any additional setup after loading the view.
    }
    
    
    
    
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo
    {
        print("----childNumber---",childNumber)
       // return IndicatorInfo(title: "\(childNumber)")
        return IndicatorInfo(title: "",image:UIImage(named: "schedul"),highlightedImage:UIImage(named: "schdul1w"))
        
       
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 166.0;//Choose your custom row height
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        
        return typeshedulearry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        // create a new cell if needed or reuse an old one
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "shedulecell") as! shedulechildacccountTableViewCell
        
        
        let temdict = scheduledict[typeshedulearry[indexPath.row]] as! NSDictionary
        
        if(temdict != nil && temdict["ScheduleSwitch"] != nil)
        {
        print("valuetempdict",temdict)
        
        let sheduleswitch = temdict["ScheduleSwitch"] as! Bool
        
        if(sheduleswitch){
            cell.sheduleswitch.isOn = true
        }
        else{
            cell.sheduleswitch.isOn = false
        }
        
        
        let weekday = temdict["Weekday"] as! NSDictionary
        let weekend = temdict["Weekend"] as! NSDictionary
        for ele in weekdayarray {
            let typedict = weekday[ele] as! NSDictionary
            print("elemen",ele)
            print("typeff",typedict)
            //var isswitch = typedict["switch"] as! Bool
            // print("isitch",isswitch)
            //            startTime = typedict["S]
            //            print("ssstrttime",startTime)
            var active = typedict["isActive"] as? Bool
            wkdystartTime = (typedict["StartTime"] as? String)!
            wkdyendTime = (typedict["EndTime"] as? String)!
            
            
            if(ele == "Tuesday"){
                
                if(active)!{
                    cell.tuesday.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
                    cell.tuesday.isSelected = true
                    roundbtn(btn: cell.tuesday)
                }
                else{
                    cell.tuesday.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
                    cell.tuesday.isSelected = false
                    roundbtn(btn: cell.tuesday)
                    
                }
                
            }
            if(ele == "Wednesday"){
                
                if(active)!{
                    cell.wednesdy.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
                    cell.wednesdy.isSelected = true
                    roundbtn(btn: cell.wednesdy)
                }
                else{
                    cell.wednesdy.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
                    cell.wednesdy.isSelected = false
                    roundbtn(btn: cell.wednesdy)
                }
                
            }
            if(ele == "Thursday"){
                
                if(active)!{
                    cell.thursday.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
                    cell.thursday.isSelected = true
                    roundbtn(btn: cell.thursday)
                }
                else{
                    cell.thursday.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
                    cell.thursday.isSelected = false
                    roundbtn(btn: cell.thursday)
                }
                
            }
            if(ele == "Friday"){
                
                if(active)!{
                    cell.fridy.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
                    cell.fridy.isSelected = true
                    roundbtn(btn: cell.fridy)
                }
                else{
                    cell.fridy.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
                    cell.fridy.isSelected = false
                    roundbtn(btn: cell.fridy)
                }
                
            }
            
            if(ele == "Monday"){
                
                
                if(active)!{
                    cell.monday.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
                    cell.monday.isSelected = true
                    roundbtn(btn: cell.monday)
                }
                else{
                    cell.monday.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
                    cell.monday.isSelected = false
                    roundbtn(btn: cell.monday)
                }
                
            }
            
            
            
            // cell.sheduleswitch.isOn = temdict["switch"] as! Bool
        }
        
        for ele in weekendarray {
            let typedict1 = weekend[ele] as! NSDictionary
            print("elemen",ele)
            print("typeff",typedict1)
            //var isswitch = typedict["switch"] as! Bool
            // print("isitch",isswitch)
            //            startTime = typedict["S]
            //            print("ssstrttime",startTime)
            var active = typedict1["isActive"] as? Bool
            wkendstartTime = (typedict1["StartTime"] as? String)!
            wkendendTime = (typedict1["EndTime"] as? String)!
            
            
            if(ele == "Saturday"){
                
                if(active)!{
                    cell.satrdy.isSelected = true
                    cell.satrdy.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
                    roundbtn(btn: cell.satrdy)
                }
                else{
                    cell.satrdy.isSelected = false
                    cell.satrdy.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
                    roundbtn(btn: cell.satrdy)
                }
                
            }
            if(ele == "Sunday"){
                
                if(active)!{
                    cell.sundy.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
                    cell.sundy.isSelected = true
                    roundbtn(btn: cell.sundy)
                }
                else{
                    cell.sundy.isSelected = false
                    cell.satrdy.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
                    roundbtn(btn: cell.sundy)
                }
                
            }
            
        }
        }
        cell.typelabelshedule.text=self.typeshedulearry[indexPath .row]
        if(subscription == "free"){
            
            if(cell.typelabelshedule.text == "BedTime"){
                cell.sheduleswitch.isEnabled = true
                cell.subImage.image = #imageLiteral(resourceName: "free")
                
                
            }
            else{
                cell.sheduleswitch.isEnabled = false
                cell.subImage.image = #imageLiteral(resourceName: "upgrade")
                cell.subImage.isHidden = false
            }
            
        }
        else if(subscription == "premium"){
            print("sss")
            cell.sheduleswitch.isEnabled = true
          //  cell.subImage.image = #imageLiteral(resourceName: "premium")
            cell.subImage.isHidden = true
        }
        
        
        cell.frmtimbtn.setTitle(wkdystartTime, for:  UIControlState.normal)
        cell.totimbtn.setTitle(wkdyendTime, for:  UIControlState.normal)
        cell.sheduleedit.addTarget(self, action: #selector(editbuttonSelected), for: .touchUpInside)
        cell.totimbtn.tag = indexPath.row
        cell.sheduleswitch.addTarget(self, action: #selector(scheSwitchAction), for: .touchUpInside)
        
//        cell.sheduleswitch.addTarget(self, action: #selector(scheSwitchAction), for: .touchDragInside)
        
        
//        cell.frmtimbtn.addTarget(self, action: #selector(buttonSelected1), for: .touchUpInside)
//        cell.totimbtn.addTarget(self, action: #selector(buttonSelected), for: .touchUpInside)
        
        
        
        return cell
        
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        CellAnimator.animateCell(cell: cell, withTransform: CellAnimator.TransformWave, andDuration: 1)
//        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
        if tableView == sheduletableview{
            
            
            let cell = sheduletableview.cellForRow(at: indexPath) as! shedulechildacccountTableViewCell
            
            let selectedItem = typeshedulearry[indexPath.row]
           // cell.contentView.backgroundColor = UIColor.white
            
            print("You tapped cell number \(indexPath.row).")
            //tableView.isHidden = true
            
            
        }
        
        
    }
    
    
    @objc func buttonSelected(sender: UIButton){
        let buttonPosition = sender.convert(CGPoint.zero, to: sheduletableview)
        let indexPath = sheduletableview.indexPathForRow(at:buttonPosition)
        let cell = sheduletableview.cellForRow(at: indexPath!) as! shedulechildacccountTableViewCell
        var labell = cell.typelabelshedule.text
        
        counter = 1
        print("hai hello button clickd")
        
        //openTimePicker()
        //        sender.setTitle(time, for: .normal)
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: 250,height: 300)
        // let pickerView = UIDatePicker(frame: CGRect(x: 0, y: 0, width: 250, height: 300))
        timePicker.datePickerMode = UIDatePickerMode.time
        timePicker.frame = CGRect(x: 0, y: 0, width: 250, height: 300)
        
        
        vc.view.addSubview(timePicker)
        let editRadiusAlert = UIAlertController(title: "Start Time", message: "", preferredStyle: UIAlertControllerStyle.alert)
        editRadiusAlert.setValue(vc, forKey: "contentViewController")
        // editRadiusAlert.addAction(UIAlertAction(title: "Set ETA", style: .default, handler: nil))
        editRadiusAlert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
        
        editRadiusAlert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction!) in
            print("you have pressed the ok button")
            
            let formatter = DateFormatter()
            formatter.timeStyle = .short
            self.time = formatter.string(from: self.timePicker.date)
            print("haiiii",self.time)
            
            //
            sender.setTitle(self.time, for: .normal)
            
            
            
            
        }))
        
        self.present(editRadiusAlert, animated: true)
        
        
        
    }
    
    @objc func buttonSelected1(sender: UIButton){
        let buttonPosition = sender.convert(CGPoint.zero, to: sheduletableview)
        let indexPath = sheduletableview.indexPathForRow(at:buttonPosition)
        
        let cell = sheduletableview.cellForRow(at: indexPath!) as! shedulechildacccountTableViewCell
        
        var labell = cell.typelabelshedule.text
        
        counter = 1
        print("hai hello button clickd")
        
        //openTimePicker()
        //        sender.setTitle(time, for: .normal)
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: 250,height: 300)
        // let pickerView = UIDatePicker(frame: CGRect(x: 0, y: 0, width: 250, height: 300))
        timePicker.datePickerMode = UIDatePickerMode.time
        timePicker.frame = CGRect(x: 0, y: 0, width: 250, height: 300)
        
        
        vc.view.addSubview(timePicker)
        let editRadiusAlert = UIAlertController(title: "End Time", message: "", preferredStyle: UIAlertControllerStyle.alert)
        editRadiusAlert.setValue(vc, forKey: "contentViewController")
        // editRadiusAlert.addAction(UIAlertAction(title: "Set ETA", style: .default, handler: nil))
        editRadiusAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        editRadiusAlert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction!) in
            print("you have pressed the ok button")
            
            let formatter = DateFormatter()
            formatter.timeStyle = .short
            self.time = formatter.string(from: self.timePicker.date)
            print("haiiii",self.time)
            
            
            sender.setTitle(self.time, for: .normal)
            
        }))
        self.present(editRadiusAlert, animated: true)
        
    }
    
    
    @objc func scheSwitchAction(sender: UISwitch){
        
        let userID12 = Auth.auth().currentUser?.uid
        ref.child("Users").child(userID12!).observe(DataEventType.value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            print("values",value)
            self.blockcount = (value?["no of blocks"] as? Int)!
            print("blockcount", self.blockcount)
            self.subscription = value?["subscription"] as? String ?? ""
            print("subscription", self.subscription)
            
            
        })
        if(subscription == "free") {
            
            let buttonPosition = sender.convert(CGPoint.zero, to: sheduletableview)
            let indexPath = sheduletableview.indexPathForRow(at:buttonPosition)
            let userID1 = Auth.auth().currentUser?.uid
            
            let cell = sheduletableview.cellForRow(at: indexPath!) as!        shedulechildacccountTableViewCell
            
            var labell = cell.typelabelshedule.text
            
            if(labell == "BedTime"){
                
                if(sender.isOn){
                    
                    switch1 = true
                }
                else{
                    switch1 = false
                    
                }
                
                self.ref.child("Children").child(userID1!).child(self.childkey).child("Master Schedule").child(cell.typelabelshedule.text!).updateChildValues(["ScheduleSwitch": switch1])
                
            }
            
            else {
                 cell.sheduleswitch.isOn = false
                cell.sheduleswitch.isEnabled = false
                 upgradeAlert(title: "Upgrade", message: "Please upgrade to premium to access this feature.")
            }
            
        }
        
        else  if(subscription == "premium") {
        let buttonPosition = sender.convert(CGPoint.zero, to: sheduletableview)
        let indexPath = sheduletableview.indexPathForRow(at:buttonPosition)
        let userID1 = Auth.auth().currentUser?.uid
        
        let cell = sheduletableview.cellForRow(at: indexPath!) as!        shedulechildacccountTableViewCell
        
        var labell = cell.typelabelshedule.text
        
        if(sender.isOn){
            
            switch1 = true
        }
        else{
            switch1 = false
            
        }
        
        self.ref.child("Children").child(userID1!).child(self.childkey).child("Master Schedule").child(cell.typelabelshedule.text!).updateChildValues(["ScheduleSwitch": switch1])
        
    }
    }
    
    
    @objc func editbuttonSelected(sender: UIButton){
        
//         ref.child("Children").child(userID1!).child(childkey).observe(DataEventType.value, with: { (snapshot) in
        
        
        
        let userID1 = Auth.auth().currentUser?.uid
        ref.child("Users").child(userID1!).observe(DataEventType.value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            print("values",value)
            self.blockcount = (value?["no of blocks"] as? Int)!
            print("blockcount", self.blockcount)
            self.subscription = value?["subscription"] as? String ?? ""
            print("subscription", self.subscription)
            
            
        })
        if(subscription == "free") {
            
            
            let buttonPosition = sender.convert(CGPoint.zero, to: sheduletableview)
            let indexPath = sheduletableview.indexPathForRow(at:buttonPosition)
            let cell = sheduletableview.cellForRow(at: indexPath!) as! shedulechildacccountTableViewCell
            
            var SheduleName = cell.typelabelshedule.text
            
            if(SheduleName == "BedTime"){
                
                let buttonPosition = sender.convert(CGPoint.zero, to: sheduletableview)
                let indexPath = sheduletableview.indexPathForRow(at:buttonPosition)
                let cell = sheduletableview.cellForRow(at: indexPath!) as! shedulechildacccountTableViewCell
                
                //let index =  sender.accessibilityHint
                //let indexPath = IndexPath.init(row: sender.tag, section: 0)
                
                print("indexedirt",indexPath)
                
                //        let cell = sheduletableview.cellForRow(at: indexPath) as! shedulechildacccountTableViewCell
                
                let res:NewScheduleViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewScheduleViewController") as!NewScheduleViewController
                
                res.name = cell.typelabelshedule.text
                res.fromtime = cell.frmtimbtn.titleLabel?.text
                res.totime = cell.totimbtn.titleLabel?.text
                res.mndaypass = cell.monday.isSelected
                res.tusdypass  = cell.tuesday.isSelected
                res.wedpass = cell.wednesdy.isSelected
                res.thrspass = cell.thursday.isSelected
                res.fridypass = cell.fridy.isSelected
                res.satrdypass = cell.satrdy.isSelected
                res.sundypass = cell.sundy.isSelected
                print("mndddy",ismonday)
                print("ttttusdy",istusdy)
                //res.url =
                self.present(res, animated: true, completion: nil)
                
                
                //cell.savebtnshedule.isHidden = false
                print("hhhhaaaaaiiiii")
                cell.frmtimbtn.isUserInteractionEnabled = true
                cell.totimbtn.isUserInteractionEnabled = true
                cell.monday.isUserInteractionEnabled = true
                cell.tuesday.isUserInteractionEnabled = true
                cell.thursday.isUserInteractionEnabled = true
                cell.wednesdy.isUserInteractionEnabled = true
                cell.fridy.isUserInteractionEnabled = true
                cell.satrdy.isUserInteractionEnabled = true
                cell.sundy.isUserInteractionEnabled = true
                cell.sheduleswitch.isUserInteractionEnabled = true
                
                
                
                
                
                
            }
            else {
                
                let alertController = UIAlertController(title: "Upgrade your Subscription", message: "Please upgrade to access this feature ", preferredStyle: .alert)
                
                let defaultAction1 = UIAlertAction(title: "CLOSE", style: .cancel, handler: nil)
                alertController.addAction(defaultAction1)
                
                // self.present(alertController, animated: true, completion: nil)
                
                
                
                let defaultAction = UIAlertAction(title: "UPGRADE", style: .default,  handler: { (action) -> Void in
                    
                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
                    
                    
                    self.navigationController?.pushViewController(vc!, animated: true)
                    
                })
                
                alertController.addAction(defaultAction)
                
                self.present(alertController, animated: true, completion: nil)
                
            }
            
            
            
            
            
        }
        else if(subscription == "premium"){
        counter = 1
        let buttonPosition = sender.convert(CGPoint.zero, to: sheduletableview)
        let indexPath = sheduletableview.indexPathForRow(at:buttonPosition)
        let cell = sheduletableview.cellForRow(at: indexPath!) as! shedulechildacccountTableViewCell
        
        //let index =  sender.accessibilityHint
        //let indexPath = IndexPath.init(row: sender.tag, section: 0)
        
        print("indexedirt",indexPath)
        
        //        let cell = sheduletableview.cellForRow(at: indexPath) as! shedulechildacccountTableViewCell
        
        let res:NewScheduleViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewScheduleViewController") as!NewScheduleViewController
        
        res.name = cell.typelabelshedule.text
        res.fromtime = cell.frmtimbtn.titleLabel?.text
        res.totime = cell.totimbtn.titleLabel?.text
        res.mndaypass = cell.monday.isSelected
        res.tusdypass  = cell.tuesday.isSelected
        res.wedpass = cell.wednesdy.isSelected
        res.thrspass = cell.thursday.isSelected
        res.fridypass = cell.fridy.isSelected
        res.satrdypass = cell.satrdy.isSelected
        res.sundypass = cell.sundy.isSelected
        print("mndddy",ismonday)
        print("ttttusdy",istusdy)
        //res.url =
        self.present(res, animated: true, completion: nil)
        
        
        //cell.savebtnshedule.isHidden = false
        print("hhhhaaaaaiiiii")
        cell.frmtimbtn.isUserInteractionEnabled = true
        cell.totimbtn.isUserInteractionEnabled = true
        cell.monday.isUserInteractionEnabled = true
        cell.tuesday.isUserInteractionEnabled = true
        cell.thursday.isUserInteractionEnabled = true
        cell.wednesdy.isUserInteractionEnabled = true
        cell.fridy.isUserInteractionEnabled = true
        cell.satrdy.isUserInteractionEnabled = true
        cell.sundy.isUserInteractionEnabled = true
        cell.sheduleswitch.isUserInteractionEnabled = true
        }
    }
    @objc func savebuttonSelected(sender: UIButton){
        
        let buttonPosition = sender.convert(CGPoint.zero, to: sheduletableview)
        let indexPath = sheduletableview.indexPathForRow(at:buttonPosition)
        let cell = sheduletableview.cellForRow(at: indexPath!) as! shedulechildacccountTableViewCell
        
        cell.sheduleedit.isHidden = false
        cell.savebtnshedule.isHidden = true
        
        cell.frmtimbtn.isUserInteractionEnabled = false
        cell.totimbtn.isUserInteractionEnabled = false
        cell.monday.isUserInteractionEnabled = false
        cell.tuesday.isUserInteractionEnabled = false
        cell.thursday.isUserInteractionEnabled = false
        cell.wednesdy.isUserInteractionEnabled = false
        cell.fridy.isUserInteractionEnabled = false
        cell.satrdy.isUserInteractionEnabled = false
        cell.sundy.isUserInteractionEnabled = false
        cell.sheduleswitch.isUserInteractionEnabled = false
        
        
    }
    
    
    
    
    
    func openTimePicker()  {
        timePicker.datePickerMode = UIDatePickerMode.time
        timePicker.frame = CGRect(x: 0.0, y: (self.view.frame.height/2 + 60), width: self.view.frame.width, height: 150.0)
        timePicker.backgroundColor = UIColor.white
        timePicker.layer.borderWidth = 3
        timePicker.layer.cornerRadius = 2
        self.view.addSubview(timePicker)
        timePicker.addTarget(self, action: #selector(childAccountViewController.startTimeDiveChanged), for: UIControlEvents.valueChanged)
    }
    
    @objc func startTimeDiveChanged(sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        time = formatter.string(from: sender.date)
        
        timePicker.removeFromSuperview() // if you want to remove time picker
        
    }
    
    
    @IBAction func addbtn(_ sender: Any) {
        
        let userID1 = Auth.auth().currentUser?.uid
        ref.child("Users").child(userID1!).observe(DataEventType.value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            print("values",value)
            self.blockcount = (value?["no of blocks"] as? Int)!
            print("blockcount", self.blockcount)
            self.subscription = value?["subscription"] as? String ?? ""
            print("subscription", self.subscription)
            
            
        })
        
         if(subscription == "free")  {
            
            upgradeAlert(title: "Upgrade your Subscription", message: "Please upgrade to access this feature")
        }
        
         else if(subscription == "premium") {
        
            
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "NewScheduleViewController") as! NewScheduleViewController
        self.present(newViewController, animated: true, completion: nil)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("hariiiiiiiiiii")
    }
    
    
    
    func roundbtn(btn: UIButton){
        
        btn.layer.cornerRadius = btn.frame.size.width / 2;
        btn.clipsToBounds = true;
        btn.layer.borderWidth = 3.0
        btn.layer.borderColor = UIColor.white.cgColor
    }
    
    func upgradeAlert(title:String,message:String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let defaultAction1 = UIAlertAction(title: "CANCEL", style: .cancel, handler: nil)
        alertController.addAction(defaultAction1)
        
        let defaultAction = UIAlertAction(title: "UPGRADE", style: .default,  handler: { (action) -> Void in
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
            
            
            self.navigationController?.pushViewController(vc!, animated: true)
            
        })
        
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
        
        
        
    }
    
    
}
