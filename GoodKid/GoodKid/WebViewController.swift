//
//  WebViewController.swift
//  GoodKid
//
//  Created by Kumar Muthusamy on 11/21/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//





import UIKit
import WebKit
import Firebase

class WebViewController: UIViewController, WKScriptMessageHandler {
    
    @IBOutlet weak var webView: WKWebView!
    //   @IBOutlet weak var webView: UIWebView!
    //var webView: WKWebView!
    var boxView = UIView()
    var textLabel = UILabel()
    var ref: DatabaseReference!
    var userID1: String = ""
    
    
    private func setupWebView() {
        
        let contentController = WKUserContentController()
        let userScript = WKUserScript(
            source: "mobileHeader()",
            injectionTime: WKUserScriptInjectionTime.atDocumentEnd,
            forMainFrameOnly: true
        )
        contentController.addUserScript(userScript)
        contentController.add(self, name: "loginAction")
        
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        
    
        
        self.webView = WKWebView(frame: self.view.bounds, configuration: config)
        print("---mobileHeader---")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.setupWebView()
        
       self.view.addSubview(self.webView)
        self.webView!.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.flexibleWidth.rawValue | UIViewAutoresizing.flexibleHeight.rawValue)

        
        let htmlpath = Bundle.main.path(forResource: "Subscribe",ofType: "html")
        let url = URL(fileURLWithPath: htmlpath!)
        let request = URLRequest(url: url)
        webView.load(request)
        
        
//        if let url = URL(string: "http://localhost:8888/web/webkitSample/index.html") {
//            let request = URLRequest(url: url)
//            self.webView.load(request)
//        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - WKScriptMessageHandler
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        DispatchQueue.main.async {
            self.lodingView(temp: true)
        }
        if message.name == "loginAction" {
            print("JavaScript is sending a message ---123\(message.body)")
            
        InAppPurchase.sharedInstance.buyAutorenewableSubscription()
            
        var childname = UserDefaults.standard.string(forKey: "childkey")
        InAppPurchase.sharedInstance.checkChildisManager(parentID: userID1,childID: childname!) { (isManaged) -> () in
          if isManaged {
            DispatchQueue.main.async {
                self.boxView.removeFromSuperview()
            }
            }

            }
            
//            DispatchQueue.main.async {
//                self.boxView.removeFromSuperview()
//            }
        }
    }
    

    
    func lodingView(temp:Bool){
        // You only need to adjust this frame to move it anywhere you want
        
        if(temp)
        {
            //            boxView = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50))
            //            textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
            boxView = UIView(frame: CGRect(x: view.frame.midX - 110, y: view.frame.midY - 25, width: 220, height: 50))
            textLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 220, height: 50))
        }else
        {
            boxView = UIView(frame: CGRect(x: view.frame.midX - 185, y: view.frame.midY - 25, width: 370, height:50))
            textLabel = UILabel(frame: CGRect(x: 36, y: 0, width: 370, height:50))
        }
        
        boxView.backgroundColor = UIColor.white
        boxView.alpha = 0.8
        boxView.layer.cornerRadius = 10
        
        //Here the spinnier is initialized
        var activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 45, height: 50)
        activityView.startAnimating()
        
        //        textLabel = UILabel(frame: CGRect(x: 36, y: 0, width: 370, height:50))
        textLabel.textColor = UIColor.gray
        //        textLabel.font = UIFont.systemFont(ofSize: 10)
        textLabel.text = "Loading"
        //        textLabel.sizeToFit()
        textLabel.adjustsFontSizeToFitWidth = true
        //        textLabel.textAlignment = .center
        
        boxView.addSubview(activityView)
        
        boxView.addSubview(textLabel)
        self.view.addSubview(boxView)
        
    }
    
}

