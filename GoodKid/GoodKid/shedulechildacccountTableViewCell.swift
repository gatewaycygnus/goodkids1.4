//
//  shedulechildacccountTableViewCell.swift
//  GoodKid
//
//  Created by Kumar Muthusamy on 10/20/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit

class shedulechildacccountTableViewCell: UITableViewCell {

    @IBOutlet weak var subImage: UIImageView!
    
    @IBOutlet weak var savebtnshedule: UIButton!
    @IBOutlet weak var typelabelshedule: UILabel!
    
    @IBOutlet weak var sheduleswitch: UISwitch!
    
    @IBOutlet weak var sheduleedit: UIButton!
    
    @IBOutlet weak var frmtimbtn: UIButton!
    
    @IBOutlet weak var totimbtn: UIButton!
    
    @IBOutlet weak var monday: UIButton!
    
    @IBOutlet weak var daysview: UIView!
    
    @IBOutlet weak var tuesday: UIButton!
    
    @IBOutlet weak var wednesdy: UIButton!
    
    
    @IBOutlet weak var thursday: UIButton!
    
    @IBOutlet weak var fridy: UIButton!
    
    @IBOutlet weak var sundy: UIButton!
    
    @IBOutlet weak var satrdy: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
