//
//  AccountViewController.swift
//  GoodKid
//
//  Created by Kumar Muthusamy on 9/19/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth



class AccountViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var editbtn: UIButton!
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var upgrade: UIButton!
    @IBOutlet weak var allowedlabel: UILabel!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var mobilenumber: UITextField!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var profileimage: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    var boxView = UIView()
    var textLabel = UILabel()
    var ref: DatabaseReference!
    var s = Userdetails.self
    var userss = [Userdetails]()
    var subscription: String = ""
    
    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
//        if #available(iOS 11.0, *) {
//            scrollView.contentInsetAdjustmentBehavior = .never
//        } else {
//            automaticallyAdjustsScrollViewInsets = false
//        }
//        
        
        
        DispatchQueue.main.async {
            self.lodingView(temp: true)
        }

        navigationController?.navigationBar.barTintColor = UIColor.orange
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        mobilenumber.delegate = self
        email.delegate = self
        username.delegate = self
        self.mobilenumber.layer.borderColor = UIColor.white.cgColor
        self.email.layer.borderColor = UIColor.white.cgColor
        self.username.layer.borderColor = UIColor.white.cgColor
        self.upgrade.layer.borderColor = UIColor.white.cgColor
        self.save.layer.borderColor = UIColor.white.cgColor
        self.mobilenumber.tintColor = UIColor.white
        self.email.tintColor = UIColor.white
        self.username.tintColor = UIColor.white
        mobilenumber.layer.borderWidth = 2
        email.layer.borderWidth = 2
        username.layer.borderWidth = 2
        upgrade.layer.borderWidth = 2
        save.backgroundColor = UIColor.white
        save.layer.cornerRadius = 20
        save.layer.borderWidth = 2
        save.isHidden = true
        profileimage.isUserInteractionEnabled = false
        
        print("----profileimage---- ",profileimage.isUserInteractionEnabled)
        
        
        
        self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "pic3"),for: .default)
        
        //        DispatchQueue.main.async {
        //            self.lodingView(temp: true)
        //        }
        //self.lodingView(temp: true)
        ref = Database.database().reference()
        let userID1 = Auth.auth().currentUser?.uid
        ref.child("Users").child(userID1!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            print("my value",value)
            let emaill = value?["email"] as? String ?? ""
            print("email",emaill)
            self.email.text = emaill
            
            let username = value?["name"] as? String ?? ""
            print("name",username)
            self.username.text = username
            self.subscription =  value?["subscription"] as? String ?? ""
       
            
            if(self.subscription.elementsEqual("free")){
                
                self.label.text = "Free Account"
                self.allowedlabel.text = "No of device allowed: 1"
                self.upgrade.isHidden = false
            }
            else if(self.subscription.elementsEqual("premium")){
                self.label.text = "Premium Account"
                self.allowedlabel.text = "No of device allowed: 5"
                self.upgrade.isHidden = true
                
            }
            
            
            let mobilenumber = value?["mobilenumber"] as? String ?? ""
            print("mobilenumber",mobilenumber)
            self.mobilenumber.text = mobilenumber
            let profileurl = value?["profileurl"] as? String ?? ""
            if(profileurl == ""){
                print("nothing")
            }else{
                let url = URL(string: profileurl)
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                self.profileimage.image = UIImage(data: data!)
                
            }
            
            DispatchQueue.main.async {
                self.boxView.removeFromSuperview()
            }
            
            // ...
        }) { (error) in
            print(error.localizedDescription)
        }
        
        upgrade.backgroundColor = UIColor.white
        upgrade.layer.cornerRadius = 20
        upgrade.layer.borderWidth = 2
        allowedlabel.backgroundColor = UIColor.white
        label.backgroundColor = UIColor.purple
        
        self.profileimage.layer.cornerRadius = self.profileimage.frame.size.width / 2;
        self.profileimage.clipsToBounds = true;
        self.profileimage.layer.borderWidth = 3.0
        self.profileimage.layer.borderColor = UIColor.white.cgColor
        
        // Do any additional setup after loading the view.
        upgrade.addTarget(self, action: #selector(upgradeaccess), for: .touchUpInside)
        
    }
    
    
    @objc func upgradeaccess(){
        print("applyaccessButton1 is applied")
        //   let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
        //  self.navigationController?.pushViewController(vc!, animated: true)
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
        self.navigationController?.pushViewController(vc!, animated: true)
      }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 10
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    
    
    @IBAction func edit(_ sender: Any) {
        username.isUserInteractionEnabled = true
        mobilenumber.isUserInteractionEnabled = true
        editbtn.isHidden = true
        save.isHidden = false
        upgrade.isHidden = true
        label.isHidden = true
        allowedlabel.isHidden = true
        profileimage.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: "imageTapped:")
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(tapGesture1))
        profileimage.addGestureRecognizer(tap1)
        
    //      profileimage.isUserInteractionEnabled = false
        print("----profileimage----1122 ",profileimage.isUserInteractionEnabled)
        
    }
    
    
    @objc func tapGesture1() {
        print("Image Tapped")
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true)
      //  profileimage.isUserInteractionEnabled = false
        
    }
    
    func lodingView(temp:Bool){
        // You only need to adjust this frame to move it anywhere you want
        
        if(temp)
        {
            boxView = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50))
            textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        }else
        {
            boxView = UIView(frame: CGRect(x: view.frame.midX - 185, y: view.frame.midY - 25, width: 370, height:50))
            textLabel = UILabel(frame: CGRect(x: 36, y: 0, width: 370, height:50))
        }
        
        boxView.backgroundColor = UIColor.white
        boxView.alpha = 0.8
        boxView.layer.cornerRadius = 10
        
        //Here the spinnier is initialized
        var activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 45, height: 50)
        activityView.startAnimating()
        
//      textLabel = UILabel(frame: CGRect(x: 36, y: 0, width: 370, height:50))
        textLabel.textColor = UIColor.gray
//      textLabel.font = UIFont.systemFont(ofSize: 10)
        textLabel.text = "Loading"
//      textLabel.sizeToFit()
        textLabel.adjustsFontSizeToFitWidth = true
        boxView.addSubview(activityView)
        boxView.addSubview(textLabel)
        self.view.addSubview(boxView)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            profileimage.image = image
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textfield: UITextField) -> Bool {
        mobilenumber.resignFirstResponder()
        username.resignFirstResponder()
        email.resignFirstResponder()
        return true;
    }
    
    
    @IBAction func Save(_ sender: Any) {
        print("hello")
        DispatchQueue.main.async {
        if let imageData = self.profileimage.image!.jpeg(.lowest) {
            
        if let image2 = self.profileimage.image
            {
        guard let data = UIImagePNGRepresentation(image2) else { return }
        let usersRef =  self.ref.child("Users")
        // let thisUserRef = usersRef.childByAutoId()
        let thisUserRef = Auth.auth().currentUser?.uid
        let storageRef2 = Storage.storage().reference().child("images").child(thisUserRef!).child("image1.png")
        storageRef2.putData(imageData, metadata: nil, completion: { (metadata, error) in
                    if error != nil
                    {
                        print("error")
                        return
                    }
            else{
            let downloadURL = metadata?.downloadURL()?.absoluteString
            let usersRef =  self.ref.child("Users")
            let thisUserRef = Auth.auth().currentUser?.uid
            let dict = ["profileurl":downloadURL]
 usersRef.child(thisUserRef!).updateChildValues(["profileurl":downloadURL])
                        
                    }
                })
            }
                print(imageData.count)
            }
        }
        
        let usersRef =  self.ref.child("Users")
        // let thisUserRef = usersRef.childByAutoId()
        let username  = self.username.text
        let mobilenumber = self.mobilenumber.text
        if(mobilenumber?.count != 10){
            
            let alertController = UIAlertController(title: "Alert", message: "Phone number is invalid", preferredStyle: .alert)
            
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        else{
            if(username?.isEmpty)!{
                
                let alertController = UIAlertController(title: "Alert", message: "Fields cannot be empty", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
            else{
                
        let thisUserRef = Auth.auth().currentUser?.uid
                
            //print("id value",thisUserRef.key)
        let dict = ["name": self.username.text, "mobilenumber": self.mobilenumber.text]
                
    usersRef.child(thisUserRef!).updateChildValues(["name": self.username.text,"mobilenumber": self.mobilenumber.text])
                
            DispatchQueue.main.async {
            if(self.subscription.elementsEqual("free")){
                self.label.text = "Free Account"
                self.allowedlabel.text = "No of device allowed:1"
                self.upgrade.isHidden = false
                }
            else if(self.subscription.elementsEqual("premium")){
                self.label.text = "Premium Account"
                self.allowedlabel.text = "No of device allowed:5"
                self.upgrade.isHidden = true
                    }
                
                self.username.isUserInteractionEnabled = false
                self.mobilenumber.isUserInteractionEnabled = false
                self.save.isHidden = true
                //  self.upgrade.isHidden = false
                self.label.isHidden = false
                self.allowedlabel.isHidden = false
                self.editbtn.isHidden = false
                self.profileimage.isUserInteractionEnabled = false
                }
            }
        }
        
        
        print("----profileimage----3344 ",profileimage.isUserInteractionEnabled)
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


extension UIView {
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
extension UILabel {
    
    
    func addBottomBorder(){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect.init(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
        bottomLine.backgroundColor = UIColor.white.cgColor
        // self.borderStyle = UILabelBorderStyle.none
        self.layer.addSublayer(bottomLine)
        
    }
}
extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return UIImageJPEGRepresentation(self, jpegQuality.rawValue)
    }
}
