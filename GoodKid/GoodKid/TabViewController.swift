//
//  TabViewController.swift
//  GoodKid
//
//  Created by cyg on 9/25/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//
import Foundation
import UIKit
import XLPagerTabStrip
import SWCombox
import Firebase
import FirebaseAuth
class TabViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, IndicatorInfoProvider {
    
    var ref: DatabaseReference!
    var selecteditem:String = ""
    var childNumber:String = ""
    var newname:String = ""
    var childName:String = ""
    var lock = true
    var blockcount:Int = 0
    var subscriptn:String = " "
    var timeinterval :String = ""
    var digit :Int = 0
    var ss : String  = ""
    var selctditem = [String]()
    var counter = 0
    var timer:Timer = Timer()
    var timer1:Timer = Timer()
    let default1 = UserDefaults.standard
    let dropDownList: [String] = ["Select the time", "15 Minutes", "30 Minutes", "1 Hour", "2 Hours", "3 Hours", "4 Hours", "Until I say so"]
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subscription: UIButton!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var dropDown: UIButton!
    @IBOutlet weak var apply: UIButton!
    @IBOutlet weak var tableView: UITableView!
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        childName = UserDefaults.standard.string(forKey: "childname")!
        let userID1 = Auth.auth().currentUser?.uid
        // userrefer  =  ref.child("users").child(userID1!)
        ref.child("Users").child(userID1!).observeSingleEvent(of: .value, with: { (snapshot) in
            var value = (snapshot.value as? NSDictionary)!
            print("my value",value)
            self.blockcount = value["no of blocks"] as! Int
            print("blockcount",self.blockcount)
            self.subscriptn = value["subscription"] as? String ?? ""
            print("sub",self.subscriptn)
        })
        self.applyRoundCorner(subscription)
        
        apply.addTarget(self, action: #selector(actionWithoutParam), for: .touchUpInside)
        subscription.addTarget(self, action: #selector(applyaccess), for: .touchUpInside)
        titleLabel.textColor = UIColor.gray
        dropDown.addTarget(self, action: #selector(dropDownTable), for: .touchUpInside)
        dropDown.backgroundColor = UIColor.white
        dropDown.titleLabel?.textAlignment = .center
        dropDown.titleLabel?.textColor = UIColor.black
        dropDown.tintColor = UIColor.black
        dropDown.titleLabel?.font = UIFont.systemFont(ofSize: 22)
        dropDown.layer.cornerRadius = 25
        dropDown.layer.borderColor = UIColor.black.cgColor
        dropDown.layer.borderWidth = 1
        tableView.isHidden = true
        
        if childNumber.elementsEqual("Grant"){
            titleLabel.text = "Grant Access"
            label.text = "How long to Grant access?"
            apply.setTitleColor(.green, for: .normal)
            
        } else if childNumber.elementsEqual("Block"){
            titleLabel.text = "Block Access"
            label.text = "How long to Block access?"
            apply.setTitleColor(.red, for: .normal)
        }
        else if childNumber.elementsEqual("Schedule")
        {
            titleLabel.text = "Schedule"
            label.isHidden = true
            apply.isHidden = true
            view1.isHidden = true
            view2.isHidden = true
            dropDown.isHidden = true
        }
        
        
        //  label.textColor = UIColor.blue
        
        view1.layer.borderColor = UIColor.gray.cgColor
        view1.layer.borderWidth = 2.0
        view1.layer.cornerRadius = 25.0
        
        // for up corner
        view1.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        view2.layer.borderColor = UIColor.gray.cgColor
        view2.layer.borderWidth = 2.0
        view2.layer.cornerRadius = 40
        // for down corner
        view2.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        //   view2.frame.intersection(view1.frame)
        
        
        //self.view1.bringSubview(toFront: view2)
        
        
    }
    
    func applyRoundCorner(_ object:AnyObject){
        if #available(iOS 12.0, *) {
            object.layer.cornerRadius = object.frame.size.width / 2
        } else {
            // Fallback on earlier versions
        }
        object.layer.masksToBounds = true
    }
    
    func applyRoundCornerContainerView(_ object:AnyObject){
        object.layer.cornerRadius = 15
        object.layer.masksToBounds = true
    }
    
    @objc func actionWithoutParam(){
        
        print("Grant or Block is applied")
    }
    
    @objc func applyaccess(){
        
        print("Subscription is applied")
    }
    
    
    
    @objc func dropDownTable(_ object:AnyObject){

        tableView.isHidden = false
        print("---dropdown clicked--- ",tableView.isHidden)
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dropDownList.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "tapcell") as UITableViewCell!
        // set the text from the data model
        cell.textLabel?.text = self.dropDownList[indexPath.row]
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = dropDownList[indexPath.row]
        dropDown.setTitle(selectedItem as String, for: UIControlState.normal)
        print("You tapped cell number \(selectedItem as String).")
        selecteditem = selectedItem as String
        var myStringArr = selecteditem.components(separatedBy: " ")
        
        if(selectedItem != "Until I say so"){
            digit = Int(myStringArr[0])!
            print("time",digit)
            timeinterval = myStringArr[1]
            print("time",timeinterval)
        }
        // UserDefaults.standard.set(selectedItem as String, forKey: "minute")
        tableView.isHidden = true
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo
    {
        print("----childNumber---",childNumber)
        return IndicatorInfo(title: "\(childNumber)")
    }
    
    @IBAction func apply(_ sender: Any) {
        
        if(childName != " "){
            print("childname------>",childName)
            if childNumber.elementsEqual("Block"){
                print("blockstate----->")
                let userID1 = Auth.auth().currentUser?.uid
                ref.child("Users").child(userID1!).observeSingleEvent(of: .value, with: { (snapshot) in
                    var value = (snapshot.value as? NSDictionary)!
                    print("my value",value)
                    self.blockcount = value["no of blocks"] as! Int
                    self.subscriptn = value["subscription"] as? String ?? ""
                })
                
                if(subscriptn == "free"){
                    print("no money iam a free user------>")
                    
                    if(blockcount < 4){
                        
                        print("block count------>",blockcount)
                        // let minute = UserDefaults.standard.string(forKey: "minute")
                        if(timeinterval == "Minutes")  && (selecteditem != "Until I say so"){
                            counter = digit * 5
                            //UserDefaults.standard.set(counter, forKey: "countersec")
                            
                            let ref = Database.database().reference()
                            print("minutes",childName)
                            let childRef =  ref.child("Children")
                            let parentid = (Auth.auth().currentUser?.uid)!
                            childRef.child(parentid).child(childName).child("lockedFlag").setValue(true)
                            blockcount += 1
                            print("block count increase------>",blockcount)
                            ref.child("Users").child(parentid).child("no of blocks").setValue(blockcount)
                            // UserDefaults.standard.set(counter, forKey: "timeinterval")
                            DispatchQueue.main.async {
                                self.timer.invalidate()
                                self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCounter), userInfo: nil, repeats: true)
                            }
                        }
                        else if( timeinterval == "Hour")  && (selecteditem != "Until I say so"){
                            
                            counter = digit * 60 * 60
                            //  UserDefaults.standard.set(counter, forKey: "countersec")
                            let ref = Database.database().reference()
                            print("hours",ss)
                            let usersRef =  ref.child("Children")
                            let parentid = (Auth.auth().currentUser?.uid)!
                            usersRef.child(parentid).child(childName).child("lockedFlag").setValue(true)
                            blockcount += 1
                            ref.child("Users").child(parentid).child("no of blocks").setValue(blockcount)
                            // UserDefaults.standard.set(counter, forKey: "timeinterval")
                            print("block count increase------>",blockcount)
                            var timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
                            
                        }
                            
                        else if( selecteditem == "Until I say so"){
                            
                            counter = 0
                            // UserDefaults.standard.set(counter, forKey: "countersec")
                            let ref = Database.database().reference()
                            print("Until I say so",ss)
                            let usersRef =  ref.child("Children")
                            let parentid = (Auth.auth().currentUser?.uid)!
                            usersRef.child(parentid).child(childName).child("lockedFlag").setValue(true)
                            blockcount += 1
                            ref.child("Users").child(parentid).child("no of blocks").setValue(blockcount)
                            print("block count increase------>",blockcount)
                        }
                    }
                        
                    else{
                        print("im traped how to block----->")
                        let alertController = UIAlertController(title: "Authentication Error", message: "In the Free Version, you are limited to 4 blocks and you have reached the limit. Please upgrade your subscription to get unlimited blocks.", preferredStyle: .alert)
                        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                        alertController.addAction(defaultAction)
                        self.present(alertController, animated: true, completion: nil)
                        
                        
                    }
                    
                    
                }
                    
                    
                else{
                    print("unlimted blocks")
                    
                    
                    if(timeinterval == "Minutes")  && (selecteditem != "Until I say so"){
                        counter = digit * 60
                        //UserDefaults.standard.set(counter, forKey: "countersec")
                        let ref = Database.database().reference()
                        print("minutes",childName)
                        let childRef =  ref.child("Children")
                        let parentid = (Auth.auth().currentUser?.uid)!
                        childRef.child(parentid).child(childName).child("lockedFlag").setValue(true)
                        blockcount += 1
                        ref.child("Users").child(parentid).child("no of blocks").setValue(blockcount)
                        // UserDefaults.standard.set(counter, forKey: "timeinterval")
                        
                        var timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
                        
                    }
                    else if( timeinterval == "Hour")  && (selecteditem != "Until I say so"){
                        
                        counter = digit * 60 * 60
                        //  UserDefaults.standard.set(counter, forKey: "countersec")
                        let ref = Database.database().reference()
                        print("hours",ss)
                        let usersRef =  ref.child("Children")
                        let parentid = (Auth.auth().currentUser?.uid)!
                        usersRef.child(parentid).child(childName).child("lockedFlag").setValue(true)
                        
                        // UserDefaults.standard.set(counter, forKey: "timeinterval")
                        
                        var timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
                        
                    }
                        
                    else if( selecteditem == "Until I say so"){
                        
                        counter = 0
                        // UserDefaults.standard.set(counter, forKey: "countersec")
                        let ref = Database.database().reference()
                        print("empty",ss)
                        let usersRef =  ref.child("Children")
                        let parentid = (Auth.auth().currentUser?.uid)!
                        usersRef.child(parentid).child(childName).child("lockedFlag").setValue(true)
                        
                    }
                    
                }
                
            }
            else if childNumber.elementsEqual("Grant"){
                
                
                if(selecteditem == "Until I say so"){
                    
                    //UserDefaults.standard.set(counter, forKey: "countersec")
                    
                    let ref = Database.database().reference()
                    print("granted",ss)
                    
                    let usersRef =  ref.child("Children")
                    let parentid = (Auth.auth().currentUser?.uid)!
                    usersRef.child(parentid).child(childName).child("lockedFlag").setValue(false)
                    DispatchQueue.main.async {
                        self.counter = 0
                        self.updateCounter()
                        
                    }
                }
            }
            
        }
            
        else{
            let alertController = UIAlertController(title: "Authentication Error", message: "Please choose child", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
            
            
        }
    }
    @objc func updateCounter() {
        //you code, this is an example
        //counter = UserDefaults.standard.integer(forKey: "countersec")
        ref = Database.database().reference()
        childName = UserDefaults.standard.string(forKey: "childname")!
        let userID1 = Auth.auth().currentUser?.uid
        // userrefer  =  ref.child("users").child(userID1!)
        ref.child("Children").child(userID1!).child(childName).observeSingleEvent(of: .value, with: { (snapshot) in
            var value = (snapshot.value as? NSDictionary)!
            print("my value",value)
            self.lock = value["lockedFlag"] as! Bool
            print("blockcount",self.blockcount)
            
        })
        
        
        if(counter == 0) || (self.lock == false){
            
            
            let ref = Database.database().reference()
            print("hhhhhhh",ss)
            counter = -1
            let usersRef =  ref.child("Children")
            let parentid = (Auth.auth().currentUser?.uid)!
            usersRef.child(parentid).child(childName).child("lockedFlag").setValue(false)
            stopTimerTest()
            
        }
        if counter > 0 {
            print("\(counter)  seconds to the end of the world")
            counter -= 1
            // UserDefaults.standard.set(counter, forKey: "countersec")
        }
    }
    
    
    func stopTimerTest() {
        print("stopTimer")
        timer.invalidate()
        
    }
    
}


