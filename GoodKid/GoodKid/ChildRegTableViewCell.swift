//
//  ChildRegTableViewCell.swift
//  GoodKid
//
//  Created by cyg on 10/5/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit

class ChildRegTableViewCell: UITableViewCell {

    @IBOutlet weak var childName: UILabel!
    
    @IBOutlet weak var childImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.childImg.layer.cornerRadius = self.childImg.frame.size.width / 2;
        self.childImg.clipsToBounds = true;
        self.childImg.layer.borderWidth = 6.0
        self.childImg.layer.borderColor = UIColor.white.cgColor
        // Do any additional setup after loading the view.
        self.childImg.layer.shadowOpacity = 0.5;
        self.childImg.layer.shadowRadius = 2.0;
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
