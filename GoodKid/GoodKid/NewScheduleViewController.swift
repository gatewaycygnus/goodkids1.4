//
//  NewScheduleViewController.swift
//  GoodKid
//
//  Created by Kumar Muthusamy on 10/27/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth
import XLPagerTabStrip

class NewScheduleViewController: UIViewController,IndicatorInfoProvider,UITextFieldDelegate
{
    
    
    @IBOutlet weak var endtm: UITextField!
    @IBOutlet weak var strttm: UITextField!
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var monday: UIButton!
    @IBOutlet weak var tuesday: UIButton!
    @IBOutlet weak var wednesday: UIButton!
    @IBOutlet weak var thursday: UIButton!
    @IBOutlet weak var friday: UIButton!
    @IBOutlet weak var saturday: UIButton!
    @IBOutlet weak var sunday: UIButton!
    @IBOutlet weak var scheduleName: UITextField!
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var deleteButton: UIButton!
    
    var name:String! = nil
    var fromtime:String! = nil
    var totime:String! = nil
    var mndaypass:Bool! = nil
    var tusdypass:Bool! = nil
    var wedpass:Bool! = nil
    var thrspass:Bool! = nil
    var fridypass:Bool! = nil
    var satrdypass:Bool! = nil
    var sundypass:Bool! = nil
    var weekdayarray = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
    var ismonday = true
    var istusday = true
    var isthursday = true
    var iswednesday = true
    var isfridy = true
    var issatrdy = false
    var issndy = false
    var active = false
    var ref: DatabaseReference!
    var counter =  1
    var time = " "
    var starttime = " "
    var endtime = " "
    let timePicker = UIDatePicker()
    var childNumber:String = ""
    var childname = " "
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        save.layer.cornerRadius = 10
        scheduleName.delegate = self
        if name != nil{
            print("hhhhsaadaddd")
            scheduleName.text = name
            strttm.text = fromtime
            print("strtm",strttm.text)
            endtm.text = totime
            print("endtm", endtm.text)
            
            monday.isSelected = mndaypass
            tuesday.isSelected = tusdypass
            wednesday.isSelected = wedpass
            thursday.isSelected = thrspass
            friday.isSelected = fridypass
            saturday.isSelected = satrdypass
            sunday.isSelected = sundypass
            
            deleteButton.isHidden = false
            
            print("mndaypass",mndaypass)
            print("tusdypass",tusdypass)
            print("wedpass",wedpass)
            print("fridypass",fridypass)
            print("satrdypass",satrdypass)
            print("sundypass",sundypass)
            
        }
        else
        {
            deleteButton.isHidden = true
        }
        
        if monday.isSelected {
            
            monday.isSelected = true
            monday.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
            roundbtn(btn: monday)
            
        }
        else{
            
            monday.isSelected = false
            monday.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
            roundbtn(btn: monday)
            
        }
        
        
        
        if tuesday.isSelected {
            
            tuesday.isSelected = true
            tuesday.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
            roundbtn(btn: tuesday)
            
        }
        else{
            
            tuesday.isSelected = false
            tuesday.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
            roundbtn(btn: tuesday)
        }
        
        
        
        
        
        if wednesday.isSelected {
            
            wednesday.isSelected = true
            wednesday.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
            roundbtn(btn: wednesday)
        }
        else{
            
            wednesday.isSelected = false
            wednesday.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
            roundbtn(btn: wednesday)
            
        }
        
        
        
        if thursday.isSelected {
            
            thursday.isSelected = true
            thursday.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
            roundbtn(btn: thursday)
            
        }
        else{
            
            thursday.isSelected = false
            thursday.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
            roundbtn(btn: thursday)
        }
        
        
        
        if friday.isSelected {
            
            friday.isSelected = true
            
            friday.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
            roundbtn(btn: friday)
            
        }
        else{
            
            friday.isSelected = false
            friday.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
            roundbtn(btn: friday)
        }
        
        
        
        if saturday.isSelected {
            
            saturday.isSelected = true
            saturday.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
            roundbtn(btn: saturday)
            
        }
        else{
            
            saturday.isSelected = false
            saturday.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
            roundbtn(btn: saturday)
        }
        
        
        if sunday.isSelected {
            
            sunday.isSelected = true
            sunday.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
            roundbtn(btn: sunday)
            
        }
        else{
            
            sunday.isSelected = false
            sunday.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
            roundbtn(btn: sunday)
        }
        
        
        
        mainview.layer.borderWidth = 3
        mainview.layer.cornerRadius = 15
        mainview.layer.borderColor = UIColor.white.cgColor
        scheduleName.layer.borderWidth = 2
        scheduleName.layer.borderColor = UIColor.black.cgColor
        strttm.layer.borderColor = UIColor.black.cgColor
        scheduleName.layer.borderColor = UIColor.black.cgColor
        strttm.layer.borderWidth = 2
        endtm.layer.borderWidth = 2
        strttm.layer.borderColor = UIColor.black.cgColor
        endtm.layer.borderColor = UIColor.black.cgColor
        
        // Do any additional setup after loading the view.
        strttm.addTarget(self, action: #selector(buttonSelected1), for: UIControlEvents.touchDown)
        
        endtm.addTarget(self, action: #selector(buttonSelected), for: UIControlEvents.touchDown)
        
        save.addTarget(self, action: #selector(saveAction), for: .touchUpInside)
        
        childname = UserDefaults.standard.string(forKey: "childkey")!
        
        
    }
    
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo
    {
        print("----childNumber---",childNumber)
        return IndicatorInfo(title: "\(childNumber)")
    }
    
    
    func textFieldShouldReturn(_ textfield: UITextField) -> Bool {
        
        scheduleName.resignFirstResponder()
        return true;
    }
    
    
    
    @objc func buttonSelected1(sender: UITextField){
        
        
        counter = 1
        print("hai hello button clickd")
        
        //     openTimePicker()
        //      sender.setTitle(time, for: .normal)
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: 250,height: 300)
        // let pickerView = UIDatePicker(frame: CGRect(x: 0, y: 0, width: 250, height: 300))
        timePicker.datePickerMode = UIDatePickerMode.time
        timePicker.frame = CGRect(x: 0, y: 0, width: 250, height: 300)
        
        vc.view.addSubview(timePicker)
        let editRadiusAlert = UIAlertController(title: "Start Time", message: "", preferredStyle: UIAlertControllerStyle.alert)
        editRadiusAlert.setValue(vc, forKey: "contentViewController")
        
        // editRadiusAlert.addAction(UIAlertAction(title: "Set ETA", style: .default, handler: nil))
        editRadiusAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        editRadiusAlert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction!) in
            print("you have pressed the ok button")
            
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            self.time = formatter.string(from: self.timePicker.date)
            print("haiiii  time----> ",self.time)
            
            
            
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            var temp = formatter.string(from: self.timePicker.date)
            var pickertime = formatter.date(from: temp)
            
            var currenttime = Date()
            print("currtent full time----> ",pickertime)
            print("currtent full time----> ",currenttime)
            if(pickertime! < currenttime)
            {
                print("you are not allowed to save this schedule...")
                
                let alertController = UIAlertController(title: "Alert", message: "Selected time elapsed, the schedule will rollover to next day. Do you want to continue?", preferredStyle: .alert)
                let defaultAction1 = UIAlertAction(title: "cancel", style: .default, handler: { (action) -> Void in
                    
                    sender.text = ""
                })
                alertController.addAction(defaultAction1)
                let defaultAction = UIAlertAction(title: "OK", style: .default,  handler: { (action) -> Void in
                    print("Ok button click...")
                    
                    self.starttime = self.time
                    sender.text = self.time
                    
                })
                
                alertController.addAction(defaultAction)
                
                self.present(alertController, animated: true, completion: nil)
                
            }
            else
            {
                self.starttime = self.time
                sender.text = self.time
            }
        }))
        self.present(editRadiusAlert, animated: true)
    }
    
    
    @objc func buttonSelected(sender: UITextField){
        
        
        counter = 1
        print("hai hello button clickd")
        
        //openTimePicker()
        //        sender.setTitle(time, for: .normal)
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: 250,height: 300)
        // let pickerView = UIDatePicker(frame: CGRect(x: 0, y: 0, width: 250, height: 300))
        timePicker.datePickerMode = UIDatePickerMode.time
        timePicker.frame = CGRect(x: 0, y: 0, width: 250, height: 300)
        
        
        vc.view.addSubview(timePicker)
        let editRadiusAlert = UIAlertController(title: "End Time", message: "", preferredStyle: UIAlertControllerStyle.alert)
        editRadiusAlert.setValue(vc, forKey: "contentViewController")
        // editRadiusAlert.addAction(UIAlertAction(title: "Set ETA", style: .default, handler: nil))
        editRadiusAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        editRadiusAlert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction!) in
            print("you have pressed the ok button")
            
            let formatter = DateFormatter()
            // formatter.timeStyle = .long
            
            
            //let dateFormatterGet = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            
            self.time = formatter.string(from: self.timePicker.date)
            print("haiiii",self.time)
            
            
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            var temp = formatter.string(from: self.timePicker.date)
            var pickertime = formatter.date(from: temp)
            
            var currenttime = Date()
            print("currtent full time----> ",pickertime)
            print("currtent full time----> ",currenttime)
            if(pickertime! < currenttime)
            {
                print("you are not allowed to save this schedule...")
                
//                let alertController = UIAlertController(title: "Confirm", message: "Are you are not allowed to save this schedule... ", preferredStyle: .alert)
                let alertController = UIAlertController(title: "Alert", message: "Selected time elapsed, the schedule will rollover to next day. Do you want to continue?", preferredStyle: .alert)
                let defaultAction1 = UIAlertAction(title: "cancel", style: .default, handler: { (action) -> Void in
                    
                    sender.text = ""
                })
                alertController.addAction(defaultAction1)
                let defaultAction = UIAlertAction(title: "OK", style: .default,  handler: { (action) -> Void in
                    print("Ok button click...")
                    
                    self.endtime = self.time
                    sender.text = self.time
                    
                })
                
                alertController.addAction(defaultAction)
                
                self.present(alertController, animated: true, completion: nil)
                
            }
            else
            {
                self.endtime = self.time
                sender.text = self.time
            }
        }))
        self.present(editRadiusAlert, animated: true)
        
        
    }
    
    
    @objc func saveAction(sender: UIButton){
        print("---saveAction applied--->")
        
        if(self.scheduleName.text == "") || (self.strttm.text == "" || endtm.text == ""){
            
            
            simpleAlert(tit: "Alert", msg: "Fields cannot be empty")
        }
            
            
        else{
            
            
            ref = Database.database().reference()
            let userID1 = Auth.auth().currentUser?.uid
            
            
            //        weakDaysList
            
            for day in weekdayarray
            {
                var tempswitch = false
                var temptype = ""
                switch(day)
                {
                case "Monday" :
                    tempswitch = monday.isSelected
                    temptype = "Weekday"
                    break
                case "Tuesday" :
                    tempswitch = tuesday.isSelected
                    temptype = "Weekday"
                    break
                case "Wednesday" :
                    tempswitch = wednesday.isSelected
                    temptype = "Weekday"
                    break
                case "Thursday" :
                    tempswitch = thursday.isSelected
                    temptype = "Weekday"
                    break
                case "Friday" :
                    tempswitch = friday.isSelected
                    temptype = "Weekday"
                    break
                case "Saturday" :
                    tempswitch = saturday.isSelected
                    temptype = "Weekend"
                    break
                case "Sunday" :
                    tempswitch = sunday.isSelected
                    temptype = "Weekend"
                    break
                default: break
                    
                }
                self.ref.child("Children").child(userID1!).child(self.childname).child("Master Schedule").child(self.scheduleName.text!).child(temptype).child(day).updateChildValues(["StartTime": self.strttm.text,"EndTime": self.endtm.text,"isActive": tempswitch])
            }
            
            
            self.ref.child("Children").child(userID1!).child(self.childname).child("Master Schedule").child(self.scheduleName.text!).updateChildValues(["ScheduleSwitch": true])
            
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    func simpleAlert(tit:String, msg:String)
    {
        let alert = UIAlertController(title: tit, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDltAction(_ sender: Any) {
        
        ref = Database.database().reference()
        let userID = Auth.auth().currentUser?.uid
        
        let alert = UIAlertController(title: "Alert", message: "Do you want to remove the schedule?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            
            self.ref.child("Children").child(userID!).child(self.childname).child("Master Schedule").child(self.scheduleName.text!).updateChildValues(["ScheduleSwitch": false])
            self.ref.child("Children").child(userID!).child(self.childname).child("Master Schedule").child(self.scheduleName.text!).removeValue()
            self.dismiss(animated: true, completion: nil)
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    @IBAction func monday(_ sender: Any) {
        
        print("dvdvvvvv",monday.isSelected)
        if monday.isSelected
        {
            
            monday.isSelected = false
            monday.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
            
        }
        else{
            monday.isSelected = true
            monday.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
            
        }
        
    }
    
    @IBAction func Tuesday(_ sender: Any) {
        
        if tuesday.isSelected {
            tuesday.isSelected = false
            tuesday.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
            
            
        }
        else{
            
            tuesday.isSelected = true
            tuesday.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
        }
    }
    
    @IBAction func weneday(_ sender: Any) {
        if wednesday.isSelected{
            wednesday.isSelected = false
            wednesday.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
            
        }
        else{
            
            wednesday.isSelected = true
            wednesday.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
        }
    }
    
    @IBOutlet weak var thursday1: UIButton!
    
    @IBAction func fridyy(_ sender: Any) {
        
        if friday.isSelected {
            
            friday.isSelected = false
            friday.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
        }
        else{
            
            friday.isSelected = true
            friday.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
        }
        
    }
    
    @IBAction func strdyy(_ sender: Any) {
        if saturday.isSelected {
            
            saturday.isSelected = false
            saturday.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
            
        }
        else{
            
            saturday.isSelected = true
            saturday.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
        }
    }
    
    @IBAction func sndaay(_ sender: Any) {
        if sunday.isSelected {
            
            sunday.isSelected = false
            sunday.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
            
        }
        else{
            
            sunday.isSelected = true
            sunday.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
        }
    }
    
    @IBAction func thrssddy(_ sender: Any) {
        print("ddddd")
        if thursday.isSelected {
            thursday.isSelected = false
            thursday.setBackgroundImage(UIImage(named: "grey"), for: UIControlState.normal)
        }
        else{
            
            thursday.isSelected = true
            thursday.setBackgroundImage(UIImage(named: "blue"), for: UIControlState.normal)
        }
    }
    
    func roundbtn(btn: UIButton)
    {
        btn.layer.cornerRadius = btn.frame.size.width / 2;
        btn.clipsToBounds = true;
        btn.layer.borderWidth = 3.0
        btn.layer.borderColor = UIColor.white.cgColor
        
    }
    
    
}



