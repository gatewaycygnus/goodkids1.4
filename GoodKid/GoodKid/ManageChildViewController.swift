//
//  ManageChildViewController.swift
//  GoodKid
//
//  Created by cyg on 9/25/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class ManageChildViewController: ButtonBarPagerTabStripViewController {
    
    override func viewDidLoad() {
         
        configureButtonBar()
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    func configureButtonBar() {
        
        settings.style.buttonBarHeight = 55
        settings.style.buttonBarBackgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        settings.style.buttonBarItemBackgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        // settings.style.selectedBarBackgroundColor = #colorLiteral(red: 0.5176470588, green: 0.06274509804, blue: 0.4901960784, alpha: 1)
        settings.style.selectedBarBackgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
       

        settings.style.buttonBarItemFont = UIFont(name: "Helvetica", size: 16.0)!
        settings.style.buttonBarItemTitleColor = .blue
        
        // Sets the pager strip item offsets
        settings.style.buttonBarMinimumLineSpacing = 5
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 5
        settings.style.buttonBarRightContentInset = 5
        
        // Sets the height and colour of the slider bar of the selected pager tab
        settings.style.selectedBarHeight = 3.0
        
        // Changing item text color on swipe
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .white
            newCell?.label.textColor = .blue
        }
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController]
    {
        let child1 = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScheduleViewController") as! ScheduleViewController
        child1.childNumber = "Schedule"
        
        let child2 = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AppRuleViewController") as! AppRuleViewController
        child2.childNumber = "App Rule"
        
        let child3 = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebFilterViewController") as! WebFilterViewController
        child3.childNumber = "Web Filter"
       //child3.backFlag = true
        return [child1, child2, child3]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
