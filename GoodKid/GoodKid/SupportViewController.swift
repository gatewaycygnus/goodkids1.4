//
//  SupportViewController.swift
//  GoodKid
//
//  Created by Kumar Muthusamy on 9/19/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit
//import SWCombox
import Firebase
import FirebaseAuth
import MessageUI


class SupportViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, MFMailComposeViewControllerDelegate,UITextViewDelegate,UITextFieldDelegate{
    var ref: DatabaseReference!
    //var email : String
    var mobilenumber = ""
    var name1 = ""
    @IBOutlet weak var submitbtn: UIButton!
    @IBOutlet weak var textviewsupport: UITextView!
    
    @IBOutlet weak var feedbck: UIButton!
    
    
    @IBOutlet weak var tableviw: UITableView!
    
    @IBOutlet weak var emailtextfild: UITextField!
    @IBOutlet weak var email: UILabel!
    let dropdown : NSMutableArray = ["Feedback","Other"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = UIColor.orange
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        self.emailtextfild.delegate = self
        self.textviewsupport.delegate = self
        tableviw.isHidden = true
        
        textviewsupport.text = "Dear Support team"
        textviewsupport.textColor = UIColor.lightGray
        submitbtn.backgroundColor = UIColor.white
        submitbtn.layer.cornerRadius = 20
        feedbck.layer.cornerRadius = 10
        emailtextfild.tintColor = UIColor.black
        textviewsupport.tintColor = UIColor.black
        
        self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "pic3"),for: .default)
        
        ref = Database.database().reference()
        let userID1 = Auth.auth().currentUser?.uid
        ref.child("Users").child(userID1!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            print("my value",value)
            let email = value?["email"] as? String ?? ""
            print("email",email)
            self.emailtextfild.text = email
            self.mobilenumber = value?["mobilenumber"] as? String ?? ""
            print("mobilenumber",email)
            
            self.name1 = value?["name"] as? String ?? ""
            print("name",email)
            //let user = User(username: username)
            
            // ...
        }) { (error) in
            print(error.localizedDescription)
        }
        
        
        
        
        
        
        func textFieldDidBeginEditing(_ textField: UITextField) {
            if textField.isFirstResponder == true {
                textField.placeholder = ""
            }
            
        }
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    
    func textFieldShouldReturn(_ textfield: UITextField) -> Bool {
        
        textfield.resignFirstResponder()
        //emailtextfild.resignFirstResponder()
        return true
    }
    
    
    
    
    
    @IBAction func Submit(_ sender: Any) {
        var name =  self.emailtextfild.text
        
        let providedEmailAddress =  self.emailtextfild.text
        let isEmailAddressValid = isValidEmailAddress(emailAddressString: providedEmailAddress!)
        
        if isEmailAddressValid
        {
            print("Email address is valid")
            if(textviewsupport.text.count != 0) && (textviewsupport.text != "Dear Support team"){
                
        var issueDetail = "Our app user needs support on " + feedbck.titleLabel!.text! + ".</br></br>The details are as follows:</br>" + textviewsupport.text
        var usrDetail = "</br></br>The customer information:</br>Name  :  \(name1) </br> Mobile:   \(mobilenumber) </br> Email  :   \(emailtextfild.text as! String)"
                let smtpSession = MCOSMTPSession()
                smtpSession.hostname = "smtp.prodentechnologies.net"
                smtpSession.username = "goodkid@prodentechnologies.net"
                smtpSession.password = "GoodCygnus$2018"
                smtpSession.port = 2525
                smtpSession.connectionLogger = {(connectionID, type, data) in
                if data != nil {
                        if let string = NSString(data: data!, encoding: String.Encoding.utf8.rawValue){
                            NSLog("Connectionlogger: \(string)")
                        }
                    }else
                    {
                        print("NO data received")
                    }
                }
                print("next received")
                let builder = MCOMessageBuilder()
                builder.header.to = [MCOAddress(displayName: "Support Team", mailbox: "goodkid@prodentechnologies.net")]
//                builder.header.to = [MCOAddress(displayName: "Support Team", mailbox: "saravanan@cygnussoftwares.com")]
                builder.header.from = MCOAddress(displayName: "Good-kidz iOS support", mailbox: "goodkid@prodentechnologies.net")
                builder.header.subject =  "Mail From  \(name1)"
                builder.htmlBody = "" + issueDetail + usrDetail
                
                let rfc822Data = builder.data()
                let sendOperation = smtpSession.sendOperation(with: rfc822Data)
                sendOperation?.start { (error) -> Void in
                    if (error != nil) {
                        NSLog("Error sending email: \(error)")
                    }
                }
                
                NSLog("Successfully sent email!")
                DispatchQueue.main.async {
                    self.showToast(controller: self, message : "Successfully sent email!", seconds: 2.0)
                }
                navigationController?.popViewController(animated: true)
                dismiss(animated: true, completion: nil)
                
            }
            else{
//                upgradeAlert(title: "Warning", message: "Please fill the field")
                
                DispatchQueue.main.async {
                    self.showToast(controller: self, message : "Please enter your comments", seconds: 2.0)
                }
                
            }
            
        } else {
            print("Email address is not valid")
            displayAlertMessage(messageToDisplay: "Email address is not valid")
        }
        
    }
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    func displayAlertMessage(messageToDisplay: String)
    {
        let alertController = UIAlertController(title: "Alert", message: messageToDisplay, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            
            // Code in this block will trigger when OK button tapped.
            print("Ok button tapped");
            
        }
        
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    func showToast(controller: UIViewController, message : String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = UIColor.black
        alert.view.alpha = 0.6
        alert.view.layer.cornerRadius = 15
        
        controller.present(alert, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            alert.dismiss(animated: true)
        }
    }
    
    
    func upgradeAlert(title:String,message:String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction1 = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(defaultAction1)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func feedback(_ sender: Any) {
        
        if tableviw.isHidden == true{
            tableviw.isHidden = false
            }else{
            tableviw.isHidden = true
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Dear Support team"
            textView.textColor = UIColor.lightGray
        }
    }
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dropdown.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell!
        
        // set the text from the data model
        cell.textLabel?.text = self.dropdown[indexPath.row] as! String
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = dropdown[indexPath.row]
        feedbck.setTitle(selectedItem as! String, for: UIControlState.normal)
        print("You tapped cell number \(indexPath.row).")
        tableView.isHidden = true
    }
    
    
}

