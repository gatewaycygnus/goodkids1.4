import UIKit
import Firebase
import CellAnimator
import FirebaseAuth

class childAccountViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, UISearchBarDelegate
{
    @IBOutlet weak var childimage: UIImageView!
    @IBOutlet weak var submit: UIButton!
    @IBOutlet weak var genderbtn: UIButton!
    @IBOutlet weak var gendrview: UITableView!
    @IBOutlet weak var agebtn: UIButton!
    @IBOutlet weak var ageview: UITableView!
    @IBOutlet weak var childname: UILabel!
    @IBOutlet weak var tablesearchbar: UISearchBar!
    @IBOutlet weak var timview: UITableView!
    
    var ref: DatabaseReference!
    var counter = 1
    var time = " "
    var bedstarttime = " "
    var bedendtime = " "
    var holidystarttime = " "
    var holidyendtime = " "
    var schoolstarttime = " "
    var schoolendtime = " "
    var weekndstarttime = " "
    var weekendtime = " "
    var gender = " "
    var age = " "
    var childkeyID = ""
    var bedsheduleswitch  = true
    var schoolsheduleswitch  = true
    var holidaysheduleswitch  = true
    var weekndsheduleswitch  = true
    
    //@IBOutlet weak var timeview: UITableView!
    let dropdown : NSMutableArray = ["Male","Female"]
    let dropdown1 : NSMutableArray = ["5","6","7","8","9","10","11","12","13","14","15","16","17"]
    let data = ["New York, NY", "Los Angeles, CA", "Chicago, IL", "Houston, TX",
                "Philadelphia, PA", "Phoenix, AZ", "San Diego, CA", "San Antonio, TX",
                "Dallas, TX", "Detroit, MI", "San Jose, CA", "Indianapolis, IN",
                "Jacksonville, FL", "San Francisco, CA", "Columbus, OH", "Austin, TX",
                "Memphis, TN", "Baltimore, MD", "Charlotte, ND", "Fort Worth, TX"]
    
    
    var typeshedulearry = [String]()
    // var filteredData = ["Bed Time","School Time"]
    let timePicker = UIDatePicker()
    var filteredData = [String]()
    var ff = [String]()
    var timeZone = [String]()
    var timeZone_gmt = [String]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tablesearchbar.delegate = self
        tablesearchbar.tintColor = UIColor.white
        submit.backgroundColor = UIColor.white
        submit.layer.cornerRadius = 20
        submit.layer.borderWidth = 2;
        tablesearchbar.layer.cornerRadius = 20
        tablesearchbar.clipsToBounds = true
        gendrview.layer.cornerRadius = 20
        ageview.layer.cornerRadius = 20
        timview.layer.cornerRadius = 20
        gendrview.layer.borderColor = UIColor.white.cgColor
        gendrview.layer.borderColor = UIColor.white.cgColor
        ageview.layer.borderColor = UIColor.white.cgColor
        submit.layer.borderColor = UIColor.white.cgColor
        
        timeZone.append("Etc/GMT+12")
        timeZone_gmt.append("(GMT-12:00) International Date Line West")
        timeZone.append("Pacific/Midway")
        timeZone_gmt.append("(GMT-11:00) Midway Island, Samoa")
        timeZone.append("Pacific/Honolulu")
        timeZone_gmt.append("(GMT-10:00) Hawaii")
        timeZone.append("US/Alaska")
        timeZone_gmt.append("(GMT-09:00) Alaska")
        timeZone.append("America/Los_Angeles")
        timeZone_gmt.append("(GMT-08:00) Pacific Time (US & Canada)")
        timeZone.append("America/Tijuana")
        timeZone_gmt.append("(GMT-08:00) Tijuana, Baja California")
        timeZone.append("US/Arizona")
        timeZone_gmt.append("(GMT-07:00) Arizona")
        timeZone.append("America/Chihuahua")
        timeZone_gmt.append("(GMT-07:00) Chihuahua, La Paz, Mazatlan")
        timeZone.append("US/Mountain")
        timeZone_gmt.append("(GMT-07:00) Mountain Time (US & Canada)")
        timeZone.append("America/Managua")
        timeZone_gmt.append("(GMT-06:00) Central America")
        timeZone.append("US/Central")
        timeZone_gmt.append("(GMT-06:00) Central Time (US & Canada)")
        timeZone.append("America/Mexico_City")
        timeZone_gmt.append("(GMT-06:00) Guadalajara, Mexico City, Monterrey")
        timeZone.append("Canada/Saskatchewan")
        timeZone_gmt.append("(GMT-06:00) Saskatchewan")
        timeZone.append("America/Bogota")
        timeZone_gmt.append("(GMT-05:00) Bogota, Lima, Quito, Rio Branco")
        timeZone.append("US/Eastern")
        timeZone_gmt.append("(GMT-05:00) Eastern Time (US & Canada)")
        timeZone.append("US/East-Indiana")
        timeZone_gmt.append("(GMT-05:00) Indiana (East)")
        timeZone.append("Canada/Atlantic")
        timeZone_gmt.append("(GMT-04:00) Atlantic Time (Canada)")
        timeZone.append("America/Caracas")
        timeZone_gmt.append("(GMT-04:00) Caracas, La Paz")
        timeZone.append("America/Manaus")
        timeZone_gmt.append("(GMT-04:00) Manaus")
        timeZone.append("America/Santiago")
        timeZone_gmt.append("(GMT-04:00) Santiago")
        timeZone.append("Canada/Newfoundland")
        timeZone_gmt.append("(GMT-03:30) Newfoundland")
        timeZone.append("America/Sao_Paulo")
        timeZone_gmt.append("(GMT-03:00) Brasilia")
        timeZone.append("America/Argentina/Buenos_Aires")
        timeZone_gmt.append("(GMT-03:00) Buenos Aires, Georgetown")
        timeZone.append("America/Godthab")
        timeZone_gmt.append("(GMT-03:00) Greenland")
        timeZone.append("America/Montevideo")
        timeZone_gmt.append("(GMT-03:00) Montevideo")
        timeZone.append("America/Noronha")
        timeZone_gmt.append("(GMT-02:00) Mid-Atlantic")
        timeZone.append("Atlantic/Cape_Verde")
        timeZone_gmt.append("(GMT-01:00) Cape Verde Is.")
        timeZone.append("Atlantic/Azores")
        timeZone_gmt.append("(GMT-01:00) Azores")
        timeZone.append("Africa/Casablanca")
        timeZone_gmt.append("(GMT+00:00) Casablanca, Monrovia, Reykjavik")
        timeZone.append("Etc/Greenwich")
        timeZone_gmt.append("(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London")
        timeZone.append("Europe/Amsterdam")
        timeZone_gmt.append("(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna")
        timeZone.append("Europe/Belgrade")
        timeZone_gmt.append("(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague")
        timeZone.append("Europe/Brussels")
        timeZone_gmt.append("(GMT+01:00) Brussels, Copenhagen, Madrid, Paris")
        timeZone.append("Europe/Sarajevo")
        timeZone_gmt.append("(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb")
        timeZone.append("Africa/Lagos")
        timeZone_gmt.append("(GMT+01:00) West Central Africa")
        timeZone.append("Asia/Amman")
        timeZone_gmt.append("(GMT+02:00) Amman")
        timeZone.append("Europe/Athens")
        timeZone_gmt.append("(GMT+02:00) Athens, Bucharest, Istanbul")
        timeZone.append("Asia/Beirut")
        timeZone_gmt.append("(GMT+02:00) Beirut")
        timeZone.append("Africa/Cairo")
        timeZone_gmt.append("(GMT+02:00) Cairo")
        timeZone.append("Africa/Harare")
        timeZone_gmt.append("(GMT+02:00) Harare, Pretoria")
        timeZone.append("Europe/Helsinki")
        timeZone_gmt.append("(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius")
        timeZone.append("Asia/Jerusalem")
        timeZone_gmt.append("(GMT+02:00) Jerusalem")
        timeZone.append("Europe/Minsk")
        timeZone_gmt.append("(GMT+02:00) Minsk")
        timeZone.append("Africa/Windhoek")
        timeZone_gmt.append("(GMT+02:00) Windhoek")
        timeZone.append("Asia/Kuwait")
        timeZone_gmt.append("(GMT+03:00) Kuwait, Riyadh, Baghdad")
        timeZone.append("Europe/Moscow")
        timeZone_gmt.append("(GMT+03:00) Moscow, St. Petersburg, Volgograd")
        timeZone.append("Africa/Nairobi")
        timeZone_gmt.append("(GMT+03:00) Nairobi")
        timeZone.append("Asia/Tbilisi")
        timeZone_gmt.append("(GMT+03:00) Tbilisi")
        timeZone.append("Asia/Tehran")
        timeZone_gmt.append("(GMT+03:30) Tehran")
        timeZone.append("Asia/Muscat")
        timeZone_gmt.append("(GMT+04:00) Abu Dhabi, Muscat")
        timeZone.append("Asia/Baku")
        timeZone_gmt.append("(GMT+04:00) Baku")
        timeZone.append("Asia/Yerevan")
        timeZone_gmt.append("(GMT+04:00) Yerevan")
        timeZone.append("Asia/Kabul")
        timeZone_gmt.append("(GMT+04:30) Kabul")
        timeZone.append("Asia/Yekaterinburg")
        timeZone_gmt.append("(GMT+05:00) Yekaterinburg")
        timeZone.append("Asia/Karachi")
        timeZone_gmt.append("(GMT+05:00) Islamabad, Karachi, Tashkent")
        timeZone.append("Asia/Calcutta")
        timeZone_gmt.append("(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi")
        timeZone.append("Asia/Calcutta")
        timeZone_gmt.append("(GMT+05:30) Sri Jayawardenapura")
        timeZone.append("Asia/Katmandu")
        timeZone_gmt.append("(GMT+05:45) Kathmandu")
        timeZone.append("Asia/Almaty")
        timeZone_gmt.append("(GMT+06:00) Almaty, Novosibirsk")
        timeZone.append("Asia/Dhaka")
        timeZone_gmt.append("(GMT+06:00) Astana, Dhaka")
        timeZone.append("Asia/Rangoon")
        timeZone_gmt.append("(GMT+06:30) Yangon (Rangoon)")
        timeZone.append("Asia/Bangkok")
        timeZone_gmt.append("(GMT+07:00) Bangkok, Hanoi, Jakarta")
        timeZone.append("Asia/Krasnoyarsk")
        timeZone_gmt.append("(GMT+07:00) Krasnoyarsk")
        timeZone.append("Asia/Hong_Kong")
        timeZone_gmt.append("(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi")
        timeZone.append("Asia/Kuala_Lumpur")
        timeZone_gmt.append("(GMT+08:00) Kuala Lumpur, Singapore")
        timeZone.append("Asia/Irkutsk")
        timeZone_gmt.append("(GMT+08:00) Irkutsk, Ulaan Bataar")
        timeZone.append("Australia/Perth")
        timeZone_gmt.append("(GMT+08:00) Perth")
        timeZone.append("Asia/Taipei")
        timeZone_gmt.append("(GMT+08:00) Taipei")
        timeZone.append("Asia/Tokyo")
        timeZone_gmt.append("(GMT+09:00) Osaka, Sapporo, Tokyo")
        timeZone.append("Asia/Seoul")
        timeZone_gmt.append("(GMT+09:00) Seoul")
        timeZone.append("Asia/Yakutsk")
        timeZone_gmt.append("(GMT+09:00) Yakutsk")
        timeZone.append("Australia/Adelaide")
        timeZone_gmt.append("(GMT+09:30) Adelaide")
        timeZone.append("Australia/Darwin")
        timeZone_gmt.append("(GMT+09:30) Darwin")
        timeZone.append("Australia/Brisbane")
        timeZone_gmt.append("(GMT+10:00) Brisbane")
        timeZone.append("Australia/Canberra")
        timeZone_gmt.append("(GMT+10:00) Canberra, Melbourne, Sydney")
        timeZone.append("Australia/Hobart")
        timeZone_gmt.append("(GMT+10:00) Hobart")
        timeZone.append("Pacific/Guam")
        timeZone_gmt.append("(GMT+10:00) Guam, Port Moresby")
        timeZone.append("Asia/Vladivostok")
        timeZone_gmt.append("(GMT+10:00) Vladivostok")
        timeZone.append("Asia/Magadan")
        timeZone_gmt.append("(GMT+11:00) Magadan, Solomon Is., New Caledonia")
        timeZone.append("Pacific/Auckland")
        timeZone_gmt.append("(GMT+12:00) Auckland, Wellington")
        timeZone.append("Pacific/Fiji")
        timeZone_gmt.append("(GMT+12:00) Fiji, Kamchatka, Marshall Is.")
        timeZone.append("Pacific/Tongatapu")
        timeZone_gmt.append("(GMT+13:00) Nuku'alofa")
        
        childname.text = UserDefaults.standard.string(forKey: "childname")
        childkeyID = UserDefaults.standard.string(forKey: "childkey")!
        let url = UserDefaults.standard.string(forKey: "childprofileurl")
        if(url != nil ){
            let url = URL(string: url!)
            let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            self.childimage.image = UIImage(data: data!)
        }
        
        ref = Database.database().reference()
        let userID1 = Auth.auth().currentUser?.uid
        ref.child("Children").child(userID1!).child(childkeyID).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            self.typeshedulearry.removeAll()
            
            let value = snapshot.value as? NSDictionary
            print("my value",value)
            
            self.gender = value?["Gender"] as? String ?? ""
            print("gender",self.gender)
            
            if self.gender.elementsEqual("Male"){
                self.childimage.image = #imageLiteral(resourceName: "icon_Little_Boy")
            } else {
                self.childimage.image = #imageLiteral(resourceName: "icon_little_girl")
            }
            
            self.genderbtn.setTitle(self.gender, for: UIControlState.normal)
            
            self.age = value?["Age"] as? String ?? ""
            print("age",self.age)
            self.agebtn.setTitle(self.age, for: UIControlState.normal)
            
            let tzone = self.timeZone_gmt[self.timeZone.index(of: value?["timezone"] as? String ?? "")!]
            self.tablesearchbar.text = tzone
            
            
        }) { (error) in
            print(error.localizedDescription)
        }
        
        timview.dataSource = self
        tablesearchbar.delegate = self
        // self.sheduletableview.layer.cornerRadius = 10.0
        self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "pic3"),for: .default)
        
        self.timview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        let knownTimeZoneIdentifiers = TimeZone.knownTimeZoneIdentifiers
        for tz in TimeZone.knownTimeZoneIdentifiers {
            let timeZone = TimeZone(identifier: tz)
            if let abbreviation = timeZone?.abbreviation(), let seconds = timeZone?.secondsFromGMT() {
                
                var sst = (tz)
                //                print("zzzzz",sst)
                if(sst != " "){
                    ff.append(sst)
                }
            }
            
            //            for element in ff {
            //                print(element)
            //            }
            
            filteredData = timeZone_gmt
        }
        gendrview.isHidden = true
        ageview.isHidden = true
        timview.isHidden = true
        tablesearchbar.text = "idlksffff"
        genderbtn.layer.cornerRadius = 10
        genderbtn.layer.borderWidth = 1
        genderbtn.layer.borderColor = UIColor.white.cgColor
        agebtn.layer.cornerRadius = 10
        agebtn.layer.borderWidth = 1
        agebtn.layer.borderColor = UIColor.white.cgColor
        
        self.childimage.layer.cornerRadius = self.childimage.frame.size.width / 2;
        self.childimage.clipsToBounds = true;
        self.childimage.layer.borderWidth = 3.0
        self.childimage.layer.borderColor = UIColor.white.cgColor
        
        // Do any additional setup after loading the view.
    }
    
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        print("end searching --> Close Keyboard")
        self.tablesearchbar.endEditing(true)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func gendrbtn(_ sender: Any) {
        
        if gendrview.isHidden == true{
            gendrview.isHidden = false
            
        }else{
            gendrview.isHidden = true
        }
    }
    
    @IBAction func agebtn(_ sender: Any) {
        
        if ageview.isHidden == true{
            ageview.isHidden = false
            
        }else{
            ageview.isHidden = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == ageview{
            
            return dropdown1.count
            
        }
        
        if tableView == timview{
            return filteredData.count
            
        }
        return dropdown.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == gendrview{
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell1") as UITableViewCell!
            cell.textLabel?.text = self.dropdown[indexPath.row] as! String
            cell.contentView.alpha = 0
            return cell
            
        }
            
        else if tableView == ageview{
            
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell2") as UITableViewCell!
            cell.textLabel?.text = self.dropdown1[indexPath.row] as! String
            return cell
        }
            
        else{
            // create a new cell if needed or reuse an old one
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell3") as UITableViewCell!
            
            // set the text from the data model
            cell.textLabel?.text = filteredData[indexPath.row] as! String
            
            return cell
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        CellAnimator.animateCell(cell: cell, withTransform: CellAnimator.TransformWave, andDuration: 1)
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == gendrview{
            let selectedItem = dropdown[indexPath.row]
            gender = selectedItem as! String
            genderbtn.setTitle(selectedItem as! String, for: UIControlState.normal)
            
            print("You tapped cell number \(indexPath.row).")
            tableView.isHidden = true
        }
        else if tableView == ageview{
            let selectedItem = dropdown1[indexPath.row]
            age = selectedItem as! String
            agebtn.setTitle(selectedItem as! String, for: UIControlState.normal)
            print("You tapped cell number \(indexPath.row).")
            tableView.isHidden = true
        }
            
        else{
            
            let selectedItem = filteredData[indexPath.row]
            tablesearchbar.text = selectedItem
            print("You tapped cell number \(indexPath.row).")
            tableView.isHidden = true
            
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        timview.isHidden = false
        filteredData = searchText.isEmpty ? timeZone_gmt : timeZone_gmt.filter { (item: String) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        
        timview.reloadData()
    }
    
    @IBAction func submit(_ sender: Any)
    {
        ref = Database.database().reference()
        let userID1 = Auth.auth().currentUser?.uid
        print("ageeeee",age)
        print("genderdddd",gender)
        
        let tzone = timeZone[timeZone_gmt.index(of:tablesearchbar.text!)!]
        ref.child("Children").child(userID1!).child(childkeyID).updateChildValues(["Gender": gender,"Age": age,"timezone": tzone])
        // Get user value
        
        let alertController = UIAlertController(title: title, message: "Saved Successfully", preferredStyle: .alert)
        let defaultAction1 = UIAlertAction(title: "OK", style: .cancel, handler:
        { (action) -> Void in
            //            self.dismiss(animated: false, completion: nil)
            self.viewDidLoad()
            self.viewWillAppear(true)
            
        })
        alertController.addAction(defaultAction1)
        self.present(alertController, animated: true, completion: nil)
        
        
        //        for element in typeshedulearry{
        //
        //            if(element == "BedTime"){
        //                self.ref.child("childs").child(userID1!).child(self.childname.text!).child("Schedule").child("BedTime").updateChildValues(["startTime": bedstarttime,"endTime": bedendtime,"switch": bedsheduleswitch])
        //
        //            }
        //            if(element == "SchoolTime")
        //            {
        //                self.ref.child("childs").child(userID1!).child(self.childname.text!).child("Schedule").child("SchoolTime").updateChildValues(["startTime": schoolstarttime,"endTime": schoolendtime,"switch": schoolsheduleswitch])
        //            }
        //
        //            if(element == "weekend")
        //            {
        //                self.ref.child("childs").child(userID1!).child(self.childname.text!).child("Schedule").child("weekend").updateChildValues(["startTime": weekndstarttime,"endTime": weekendtime,"switch": weekndsheduleswitch])
        //
        //            }
        //            if(element == "Holiday")
        //            {
        //                self.ref.child("childs").child(userID1!).child(self.childname.text!).child("Schedule").child("Holiday").updateChildValues(["startTime": holidystarttime,"endTime": holidyendtime,"switch": holidaysheduleswitch])
        //
        //            }
        //        }
        
    }
    
    
    
    func openTimePicker()
    {
        timePicker.datePickerMode = UIDatePickerMode.time
        timePicker.frame = CGRect(x: 0.0, y: (self.view.frame.height/2 + 60), width: self.view.frame.width, height: 150.0)
        timePicker.backgroundColor = UIColor.white
        timePicker.layer.borderWidth = 3
        timePicker.layer.cornerRadius = 2
        self.view.addSubview(timePicker)
        timePicker.addTarget(self, action: #selector(childAccountViewController.startTimeDiveChanged), for: UIControlEvents.valueChanged)
    }
    
    @objc func startTimeDiveChanged(sender: UIDatePicker)
    {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        time = formatter.string(from: sender.date)
        timePicker.removeFromSuperview() // if you want to remove time picker
    }
    
    @IBAction func deletebtn(_ sender: Any) {
        let alertController = UIAlertController(title: "Alert", message: "Do you want to remove this device from management?", preferredStyle: .alert)
        let defaultAction1 = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
        alertController.addAction(defaultAction1)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default,  handler: { (action) -> Void in
            let userID1 = Auth.auth().currentUser?.uid
            self.ref.child("Children").child(userID1!).child(self.childkeyID).removeValue()
            self.ref.child("ChildInfo").child(userID1!).child(self.childkeyID).removeValue()
            print("keyvalueeeeee",UserDefaults.standard.string(forKey: "childkey"))
            UserDefaults.standard.removeObject(forKey: "childkey")
            print("keyvalueeeeee22",UserDefaults.standard.string(forKey: "childkey"))
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ManageChildStackViewController") as? ManageChildStackViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            
            
        })
        
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    
    
    
}


