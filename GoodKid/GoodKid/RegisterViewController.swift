//
//  RegisterViewController.swift
//  GoodKid
//
//  Created by Kumar Muthusamy on 9/14/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth
class RegisterViewController: UIViewController , UITextFieldDelegate {
    //@IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var register: UIButton!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var conformpassword: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var mobilenumber: UITextField!
    @IBOutlet weak var name: UITextField!
    

    var ref: DatabaseReference!
    
    var userdetail = [Userdetails]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = UIColor.orange
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        email.layer.borderColor = UIColor.white.cgColor
        email.layer.borderWidth = 2
        email.tintColor = UIColor.white
        conformpassword.layer.borderColor = UIColor.white.cgColor
        conformpassword.layer.borderWidth = 2
        conformpassword.tintColor = UIColor.white
        password.layer.borderColor = UIColor.white.cgColor
        password.layer.borderWidth = 2
        password.tintColor = UIColor.white
        mobilenumber.layer.borderColor = UIColor.white.cgColor
        mobilenumber.layer.borderWidth = 2
        mobilenumber.tintColor = UIColor.white
        name.layer.borderColor = UIColor.white.cgColor
        name.layer.borderWidth = 2
        name.tintColor = UIColor.white
        register.backgroundColor = UIColor.white
        register.layer.borderColor = UIColor.white.cgColor
        register.layer.cornerRadius = 20
        register.layer.borderWidth = 2
       
        name.delegate = self
        mobilenumber.delegate = self
    mobilenumber.tag = 2
        password.delegate = self
        conformpassword.delegate = self
        email.delegate = self
        self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "pic3"),for: .default)
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    func textFieldShouldReturn(_ textfield: UITextField) -> Bool {
      name.resignFirstResponder()
      mobilenumber.resignFirstResponder()
      email.resignFirstResponder()
      password.resignFirstResponder()
      conformpassword.resignFirstResponder()
        return true;
    }
    
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField.tag == 2){
        let maxLength = 10
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
        }
        let maxLength = 30
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func register(_ sender: Any) {
        
        if email.text == "" || password.text == "" || name.text == ""  || mobilenumber.text == "" || conformpassword.text == "" {
            
            
            
            let alertController = UIAlertController(title: "Alert", message: "Fields cannot be empty", preferredStyle: .alert)
            
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
            
        }
        
        else {
           
            
            //check whether the password and conform password are same
            
            if self.password.text != self.conformpassword.text{
                
                //  if it is not same show the alert
                let alertController = UIAlertController(title: "Alert", message: "Passwords Mismatch", preferredStyle: .alert)
                
                
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                
                self.present(alertController, animated: true, completion: nil)
    
                
                print("password same")
                
            }
        
        else {
                if mobilenumber.text?.count != 10{
                    
                    let alertController = UIAlertController(title: "Alert", message: "Phone number is invalid", preferredStyle: .alert)
                    
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                
                
                else{
                
              print("emaillll",email.text)
            Auth.auth().createUser(withEmail: email.text!, password: password.text!) { (user, error) in
                
                if error == nil {
                    print("You have successfully signed up")
                    //Goes to the Setup page which lets the user take a photo for their profile picture and also chose a username
                    
                    
                    //database reference
                    
                    self.ref = Database.database().reference()
                    
                    //SystemDate----------------->
                    let date : Date = Date()
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MMM dd, yyyy"
                    let todaysDate = dateFormatter.string(from: date)
                    
                    //----------------------------->
                    
                    let  noofdevice = "1"
                    let noofblock = 0
                    let subscription = "free"
                    let id = ""
                    let noofgrants = 0
                    //----------->passing values to the data model--------->
              let profileurl = ""
                    var s = Userdetails(name: self.name.text!,email: self.email.text!,password:self.password.text!, mobilenumber: self.mobilenumber.text!,signupdate: todaysDate,numberofdevices: noofdevice,noofBlocks: noofblock,Subscripition: subscription,profileurl: profileurl,noofgrants: noofgrants)
                    
                    //------------------------------------------------   
                    s.saveToFirebase()

                    self.navigationController?.popViewController(animated: true)
                    
                   self.dismiss(animated: true, completion: nil)

                    
                    
                } else {
                    let alertController = UIAlertController(title: "Alert", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                     }
                }
        }
        
        
        }
        

        var Name = name.text
        var Email = email.text
        var Mobile = mobilenumber.text
        var Password = password.text
        print("--------------->details------->",Name,Email,Mobile,Password)
        
        
    }
    
    
}




