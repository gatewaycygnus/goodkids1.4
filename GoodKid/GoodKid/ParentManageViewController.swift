//
//  parentViewController.swift
//  GoodKid
//
//  Created by Kumar Muthusamy on 10/19/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit
import Firebase
import CellAnimator
import FirebaseAuth

class ParentManageViewController: UIViewController {
    
    @IBOutlet weak var statusimage: UIImageView!
    @IBOutlet weak var childinfo: UIButton!
    @IBOutlet weak var childselection: UIBarButtonItem!
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var childimage: UIImageView!
    @IBOutlet weak var childname: UILabel!
    @IBOutlet weak var blockbtn: UIButton!
    @IBOutlet weak var grantbtn: UIButton!
    @IBOutlet weak var grntbtn: UIButton!
    
    var strmail:String! = nil
    var url:String! = nil
    var usersRef =  ""
    var parentid = " "
    var ref: DatabaseReference!
    var lockedflg = true
    var gender = " "
    var boxView = UIView()
    var textLabel = UILabel()
    var blockcount = 0
    var grantcount = 0
    var subscription = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.lodingView(temp: true)
        }
        
        print("gotit")
        ref = Database.database().reference()
        let userID1 = Auth.auth().currentUser?.uid
        ref.child("Users").child(userID1!).observe(DataEventType.value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            print("values",value)
            self.blockcount = (value?["no of blocks"] as? Int)!
            print("blockcount", self.blockcount)
            self.grantcount = (value?["noofgrants"] as? Int)!
            self.subscription = value?["subscription"] as? String ?? ""
            print("subscription", self.subscription)
            UserDefaults.standard.set(self.subscription, forKey: "subscription")
            
        })
        
        print("childkey",UserDefaults.standard.string(forKey: "childkey"))
        if(UserDefaults.standard.string(forKey: "childkey")! != nil){
             ref.child("Children").child(userID1!).child(UserDefaults.standard.string(forKey: "childkey")!).observe(DataEventType.value, with: { (snapshot) in
                // Get user value
                print("snpstkey",snapshot.key)
                print("snpstkey",snapshot.value)
                if(snapshot.value != nil){
                    let value = snapshot.value as? NSDictionary
                    print("values",value)
                    if(value != nil){
                        self.lockedflg = (value?["LockedFlag"] as? Bool)!
                        self.gender = (value?["Gender"] as? String)!
                        
                        
                        if self.gender.elementsEqual("Male"){
                            self.childimage.image = #imageLiteral(resourceName: "icon_Little_Boy")
                        } else {
                            self.childimage.image = #imageLiteral(resourceName: "icon_little_girl")
                        }
                        
                        if(self.lockedflg){
                            
                            self.statusimage.image = UIImage(named: "red.png") as! UIImage
                        }
                        else{
                            
                            self.statusimage.image = UIImage(named: "grn.png") as! UIImage
                            
                            
                        }
                    }
                }
            })
            
        }
        else{
            print("no child")
        }
        
        
        //        DispatchQueue.main.async {
        //            self.lodingView(temp: true)
        //        }
        
        // self.lodingView(temp: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        var image = UIImage(named: "pic3") as! UIImage
        self.navigationController?.navigationBar.setBackgroundImage(image,for: .default)
        
        //let logo =
        // let imageView = UIImageView(image:logo)
        //childselection.image = UIImage(named: "asd")
        
        
        
        var usersRef =  ref.child("Children")
        parentid = (Auth.auth().currentUser?.uid)!
        
        
        childname.text = UserDefaults.standard.string(forKey: "childname")
        print("---parentManageView--- childName",UserDefaults.standard.string(forKey: "childname"))
        let profileurl = UserDefaults.standard.string(forKey: "url")
        
        if(profileurl == ""){
            print("nothing")
        }
        else{
            let url = URL(string: profileurl!)
            let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            //Neelavathi
            //   self.childimage.image = UIImage(data: data!)
        }
        
        DispatchQueue.main.async {
            self.boxView.removeFromSuperview()
        }
        
        
        
        
        self.childimage.layer.cornerRadius = self.childimage.frame.size.width / 2;
        self.childimage.clipsToBounds = true;
        self.childimage.layer.borderWidth = 6.0
        self.childimage.layer.borderColor = UIColor.white.cgColor
        // Do any additional setup after loading the view.
        self.childimage.layer.shadowOpacity = 0.5;
        self.childimage.layer.shadowRadius = 2.0;
        
        blockbtn.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        
        
        grantbtn.roundCorners(corners: [.topLeft], radius: 500)
        grantbtn.roundCorners(corners: [.topRight], radius: 500)
        
        
        blockbtn.roundCorners(corners: [.bottomRight], radius: 500)
        blockbtn.roundCorners(corners: [.bottomLeft], radius: 500)
        
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ManageChildViewController") as! ManageChildViewController
        vc.view.frame = self.view2.bounds
        
        let n = UserDefaults.standard.string(forKey: "childkey")
        print("lllllddddll",n)
        self.addChildViewController(vc)
        view2.addSubview(vc.view)
        vc.didMove(toParentViewController: self) //OR  vc.willMove(toParentViewController: self)
        
    }
    
    func lodingView(temp:Bool){
        // You only need to adjust this frame to move it anywhere you want
        
        if(temp)
        {
            boxView = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50))
            textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        }else
        {
            boxView = UIView(frame: CGRect(x: view.frame.midX - 185, y: view.frame.midY - 25, width: 370, height:50))
            textLabel = UILabel(frame: CGRect(x: 36, y: 0, width: 370, height:50))
        }
        
        boxView.backgroundColor = UIColor.white
        boxView.alpha = 0.8
        boxView.layer.cornerRadius = 10
        
        //Here the spinnier is initialized
        var activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 45, height: 50)
        activityView.startAnimating()
        
        //        textLabel = UILabel(frame: CGRect(x: 36, y: 0, width: 370, height:50))
        textLabel.textColor = UIColor.gray
        //        textLabel.font = UIFont.systemFont(ofSize: 10)
        textLabel.text = "Loading"
        //        textLabel.sizeToFit()
        textLabel.adjustsFontSizeToFitWidth = true
        
        boxView.addSubview(activityView)
        
        boxView.addSubview(textLabel)
        self.view.addSubview(boxView)
        
    }
    
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func childinfo(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "childAccountViewController") as? childAccountViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
        print("hhari")
    }
    
    @IBAction func blckbtn(_ sender: Any) {
        
        
        let alertController = UIAlertController(title: "Confirm", message: "Do you want to block the child? ", preferredStyle: .alert)
        
        let defaultAction1 = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
        alertController.addAction(defaultAction1)
        
        // self.present(alertController, animated: true, completion: nil)
        
        
        
        let defaultAction = UIAlertAction(title: "OK", style: .default,  handler: { (action) -> Void in
            print("Ok button click...")
            
            let userID1 = Auth.auth().currentUser?.uid
            self.ref.child("Users").child(userID1!).observe(DataEventType.value, with: { (snapshot) in
                // Get user value
                let value = snapshot.value as? NSDictionary
                print("values",value)
                self.blockcount = (value?["no of blocks"] as? Int)!
                print("blockcount", self.blockcount)
                self.subscription = value?["subscription"] as? String ?? ""
                print("subscription", self.subscription)
                
                
            })
            if(self.subscription == "free"){
                print("blockcount", self.blockcount)
                if(self.blockcount < 5){
                    self.ref = Database.database().reference()
                    let usersRef =  self.ref.child("Children")
                    let parentid = (Auth.auth().currentUser?.uid)!
                    usersRef.child(parentid).child( UserDefaults.standard.string(forKey: "childkey")!).child("LockedFlag").setValue(true)
                    
                    self.blockcount = self.blockcount + 1
                    
                    self.ref.child("Users").child(parentid).updateChildValues(["no of blocks": self.blockcount])
                    print("bloxckzz",self.blockcount)
                    
                    self.simpleAlert(tit: "Alert", msg: "Blocked Successfully.")
                    
                    
                }
                else {
                    let alertController = UIAlertController(title: "Upgrade your Subscription", message: "Please upgrade to access this feature ", preferredStyle: .alert)
                    
                    let defaultAction1 = UIAlertAction(title: "CANCEL", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction1)
                    
                    // self.present(alertController, animated: true, completion: nil)
                    
                    
                    
                    let defaultAction = UIAlertAction(title: "UPGRADE", style: .default,  handler: { (action) -> Void in
                        
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
                        
                        
                        
                        self.navigationController?.pushViewController(vc!, animated: true)
                        
                    })
                    
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                }
                
            }
                
            else if (self.subscription == "premium"){
                self.ref = Database.database().reference()
                let usersRef =  self.ref.child("Children")
                let parentid = (Auth.auth().currentUser?.uid)!
                usersRef.child(parentid).child( UserDefaults.standard.string(forKey: "childkey")!).child("LockedFlag").setValue(true)
                self.simpleAlert(tit: "Alert", msg: "Blocked Successfully.")
                
            }
            
            
            // self.logoutFun()
        })
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func grntbtn1(_ sender: Any) {
        
        
        let alertController = UIAlertController(title: "Confirm", message: "Do you want to grant the child? ", preferredStyle: .alert)
        
        let defaultAction1 = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
        alertController.addAction(defaultAction1)
        
        // self.present(alertController, animated: true, completion: nil)
        
        
        
        let defaultAction = UIAlertAction(title: "OK", style: .default,  handler: { (action) -> Void in
            print("Ok button click...")
            let userID1 = Auth.auth().currentUser?.uid
            
            DispatchQueue.main.async {
                self.ref.child("Users").child(userID1!).observe(DataEventType.value, with: { (snapshot) in
                    // Get user value
                    let value = snapshot.value as? NSDictionary
                    print("values",value)
                    self.grantcount = (value?["noofgrants"] as? Int)!
                    print("grantcounttt", self.grantcount)
                    self.subscription = value?["subscription"] as? String ?? ""
                    print("subscription", self.subscription)
                    
                })
            }
            if(self.subscription == "free"){
                
                if(self.grantcount < 5){
                    self.ref = Database.database().reference()
                    let usersRef =  self.ref.child("Children")
                    let parentid = (Auth.auth().currentUser?.uid)!
                    usersRef.child(parentid).child( UserDefaults.standard.string(forKey: "childkey")!).child("LockedFlag").setValue(false)
                    self.grantcount = self.grantcount + 1
                    self.ref.child("Users").child(parentid).updateChildValues(["noofgrants": self.grantcount])
                    print("bloxckzz",self.grantcount)
                    self.simpleAlert(tit: "Alert", msg: "Granted Successfully.")
                    
                }
                else{
                    
                    self.upgradeAlert(title: "Upgrade your Subscription", message: "Please upgrade to access this feature")
                }
                
            }
                
            else if (self.subscription == "premium"){
                self.ref = Database.database().reference()
                let usersRef =  self.ref.child("Children")
                let parentid = (Auth.auth().currentUser?.uid)!
                usersRef.child(parentid).child( UserDefaults.standard.string(forKey: "childkey")!).child("LockedFlag").setValue(false)
                self.simpleAlert(tit: "Alert", msg: "Granted Successfully.")
            }
            
        })
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    func upgradeAlert(title:String,message:String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction1 = UIAlertAction(title: "CANCEL", style: .cancel, handler: nil)
        alertController.addAction(defaultAction1)
        let defaultAction = UIAlertAction(title: "UPGRADE", style: .default,  handler: { (action) -> Void in
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            
        })
        
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    func simpleAlert(tit:String, msg:String)
    {
        let alert = UIAlertController(title: tit, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    @IBAction func childeselection(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "childselectionViewController") as! childselectionViewController
        self.present(newViewController, animated: true, completion: nil)
        
        
    }
    
    
    @IBAction func Back(_ sender: UIBarButtonItem) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ManageChildStackViewController") as? ManageChildStackViewController
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    //    @IBAction func back(_ sender: Any) {
    //
    //
    //        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ManageChildStackViewController") as? ManageChildStackViewController
    //
    //        self.navigationController?.pushViewController(vc!, animated: true)
    //
    //    }
    
}

extension UIButton {
    func addTopBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        
        border.frame = CGRect(x:0,y: 0, width:self.frame.size.width, height:width)
        self.layer.addSublayer(border)
    }
    
    func addRightBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        
        border.frame = CGRect(x: self.frame.size.width - width,y: 0, width:width, height:self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0, y:self.frame.size.height - width, width:self.frame.size.width, height:width)
        self.layer.addSublayer(border)
    }
    
    func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        
        // border.masksToBounds = true
        border.frame = CGRect(x:0, y:0, width:width, height:self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    
    
    
}

