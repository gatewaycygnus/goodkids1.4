//
//  childselectionTableViewCell.swift
//  GoodKid
//
//  Created by Kumar Muthusamy on 11/10/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit

class childselectionTableViewCell: UITableViewCell {

    @IBOutlet weak var childname: UILabel!
    @IBOutlet weak var childimage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.childimage.layer.cornerRadius = self.childimage.frame.size.width / 2;
        self.childimage.clipsToBounds = true;
        self.childimage.layer.borderWidth = 6.0
        self.childimage.layer.borderColor = UIColor.white.cgColor
        // Do any additional setup after loading the view.
        self.childimage.layer.shadowOpacity = 0.5;
        self.childimage.layer.shadowRadius = 2.0;
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
