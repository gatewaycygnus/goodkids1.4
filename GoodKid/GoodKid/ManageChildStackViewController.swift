//
//  ManageChildStackViewController.swift
//  GoodKid
//
//  Created by cyg on 10/7/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import WebKit
class ManageChildStackViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var regChild: UIButton!
    @IBOutlet weak var regNewChild: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableView1: UITableView!
    @IBOutlet weak var unRegChild: UIButton!
  @IBOutlet weak var regInstruction: UIMarginLabel!
   // @IBOutlet weak var htmllpage: UILabel!
    @IBOutlet weak var label1: UIMarginLabel!
    @IBOutlet weak var label2: UIMarginLabel!
    @IBOutlet weak var label3: UIMarginLabel!
    @IBOutlet weak var label4: UIMarginLabel!
    @IBOutlet weak var regInstruction2: UIMarginLabel!
    @IBOutlet weak var regInstruction3: UIMarginLabel!
    @IBOutlet weak var regInstruction4: UIMarginLabel!
    
   
    
    //@IBOutlet weak var webview: WKWebView!
    var childRegisterList = [String]()
    var childgenderList = [String]()
    var childKeyList = [String]()
    var childimgeList = [String]()
    var ref: DatabaseReference!
    var userID1: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let htmlpath = Bundle.main.path(forResource: "instruction",ofType: "html")
        let url = URL(fileURLWithPath: htmlpath!)
        let request = URLRequest(url: url)
       // webview.load(request)
//        var htmlText:String = "<html><body>
//        <h4 style="color:black"><em> Step1: Configure child's device to Goodkid</em></h4>
//        <ol style="color: " >
//        
//        <li>Make sure that iTunes is installed on your PC.</li> <br>
//        <li>Visit  <a href="http://183.82.251.43:8070/Goodkid_web/">http://183.82.251.43:8070/Goodkid_web/</a>  and download our Goodkid device configurator to configure child’s device.</li>
//        
//        </ol>
//        
//        
//        <h4 style="color:black"><em>Step2: Install Mobile Device Management Profile</em></h4>
//        
//        <ol>
//        
//        <li>On your child's device, open Safari and go to <a href="https://183.82.251.43:8080">https://183.82.251.43:8080</a> </li>
//        <br>
//        <li>Login using your goodkid account.</li><br>
//        <li>Provide child name and other needed information.</li><br>
//        <li>Press Pair and install profile.</li><br>
//        <li>Now you can control your child’s device from your iOS app.</li>
//        
//        </ol>
//        
//        
//        </body>
//        </html>
//        "
//        htmllpage.text = htmlText.htmlToString
        
        // self.lodingView(temp: true)
        
 DispatchQueue.main.async {
            self.lodingView(temp: true)
        }
        
        self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "pic3"),for: .default)
        ref = Database.database().reference()
        userID1 = (Auth.auth().currentUser?.uid)!
        UserDefaults.standard.set(userID1, forKey: "parentid")
        
        ref.child("Children").child(userID1).observe(.value, with: {
            snapshot in
            
            self.childRegisterList.removeAll()
            self.childKeyList.removeAll()
            self.childimgeList.removeAll()
            self.childgenderList.removeAll()
            
            for group in snapshot.children {
                
            self.ref.child("Children").child(self.userID1).child((group as AnyObject).key).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    print("name---value---->555 )",snapshot.key)
                    self.childKeyList.append(snapshot.key)
                    let value = snapshot.value as? NSDictionary
                    let profileurl = value?["profileURL"] as? String ?? ""
                    let name = value?["Name"] as? String ?? ""
                    let gender = value?["Gender"] as? String ?? ""
                    self.childRegisterList.append(name)
                    self.childgenderList.append(gender)
                    self.tableView1.reloadData()
                    self.tableView.reloadData()
                    print("name--->1111",name)
                    print("name--->2222",self.childRegisterList)
                    self.childimgeList.append(profileurl)
                    print("ggg",self.childRegisterList.count)
                    print("hari",self.childimgeList.count)
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.tableView1.reloadData()
                        print("name",self.childRegisterList.count)
                        print("image",self.childimgeList.count)
                    }
                })
            }
            
        var usersRef = self.ref.child("Children")
        usersRef.observe(.value, with: { snapshot in
        print("---snapshot webfilter ",snapshot.hasChild(self.userID1))
        if snapshot.hasChild(self.userID1){
                    self.tableView.isHidden = false
                    self.tableView1.isHidden = true
                    self.regChild.isHidden = false
                    self.regNewChild.isHidden = false
                    self.unRegChild.isHidden = false
                    self.regInstruction.isHidden = true
                    self.regInstruction2.isHidden = true
                    self.regInstruction3.isHidden = true
                    self.regInstruction4.isHidden = true
                    self.label1.isHidden = true
                    self.label2.isHidden = true
                    self.label3.isHidden = true
                    self.label4.isHidden = true
                    
                } else {
                    self.tableView.isHidden = true
                    self.regInstruction.isHidden = true
                    self.regInstruction2.isHidden = true
                    self.regInstruction3.isHidden = true
                    self.regInstruction4.isHidden = true
                    self.tableView1.isHidden = true
                    self.regChild.isHidden = true
                    self.regNewChild.isHidden = true
                    self.unRegChild.isHidden = true
                    
                    self.label1.isHidden = false
                    self.label2.isHidden = false
                    self.label3.isHidden = false
                    self.label4.isHidden = false
                    DispatchQueue.main.async {
                        self.boxView.removeFromSuperview()
                    }
                    
                    print("---No WebFilters--- ")
                }
                
            })
            print("self.childRegisterList.count-----> ",self.childRegisterList.count)
            
        })
        
        
        // Do any additional setup after loading the view.
        
        self.addSlideMenuButton()
        
        titleLabel.text = "Welcome to Good-kidz Parental Control App!"
        
        
        titleLabel.adjustsFontSizeToFitWidth = false
        
        regChild.setTitle("     CHOOSE A DEVICE FROM BELOW TO MANAGE      ", for: .normal)
       
        regChild.titleLabel?.textAlignment = .center
        regChild.layer.cornerRadius = 20
        regChild.titleLabel?.numberOfLines = 0
        
        regNewChild.setTitle("TO REGISTER A NEW CHILD DEVICE", for: .normal)
       
        regNewChild.titleLabel?.textAlignment = .center
        regNewChild.layer.cornerRadius = 20
        
        
        
        //  regInstruction.text = "Step1: Configure child's device to Goodkid\n\n  1.Make sure that iTunes is installed on your PC.\n  2.Visit www.goodkid.com/downloads and download our Goodkid device configurator to configure child’s device.\n\nStep2: Install Mobile Device Management Profile\n\n  1.On your child's device, open Safari and go to control.proden.com.\n  2.Login to your goodkid account.\n  3.Provide his/her name and other needed information.\n  4.Press Pair and install profile.\n  5.Now you can control your child’s device from your iOS app."
        
    //    regInstruction.text = "1. Make sure that iTunes is installed on your PC.\n\n2. Visit http://183.82.251.43:8070/Goodkid_web/ and download our Goodkid device configurator to configure child’s device.\n\n3. On your child's device, open Safari and go to https://183.82.251.43:8080\n\n4. Login using your goodkid account.\n\n5. Provide child name and other needed information.\n\n6. Press Pair and install profile.\n\n7. Now you can control your child’s device from your iOS app."
        
        
        regInstruction.text = "Step 1: Configure child's device to Good-kidz manager"
       
        regInstruction2.text = "1. Make sure that iTunes is installed on your PC.\n\n2. Visit https://good-kidz.com/downloads and download the Good-kidz device configurator to configure child’s device."
     
        regInstruction3.text = "Step 2: Install Mobile Device Management Profile"
        
        regInstruction4.text = "1. On your child's device, open Safari and go to https://good-kidz.com\n\n2. Login using your Good-kidz account.\n\n3. Provide child name and other needed information.\n\n4. Press Pair and install profile.\n\n5. Now you can control your child’s device from your iOS app."
        

        
        regInstruction.leftInset = 10
        regInstruction.rightInset = 10
        regInstruction.sizeToFit()
        regInstruction.adjustsFontSizeToFitWidth = true
        
        regInstruction2.leftInset = 10
        regInstruction2.rightInset = 10
        regInstruction2.sizeToFit()
        regInstruction2.adjustsFontSizeToFitWidth = true
        regInstruction3.leftInset = 10
        regInstruction3.rightInset = 10
        regInstruction3.sizeToFit()
        regInstruction3.adjustsFontSizeToFitWidth = true
        regInstruction4.leftInset = 10
        regInstruction4.rightInset = 10
        regInstruction4.sizeToFit()
        regInstruction4.adjustsFontSizeToFitWidth = true
        
        
        unRegChild.setTitle(" UN REGISTER A CHILD DEVICE (THAT IS REGISTERED ALREADY)", for: .normal)
        
        unRegChild.titleLabel?.textAlignment = .center
        unRegChild.layer.cornerRadius = 20
        
        
        label1.text = "Step 1: Configure child's device to Good-kidz"
        label1.adjustsFontSizeToFitWidth = true
        label1.leftInset = 10
        label1.rightInset = 10
        
        label2.text = "1. Make sure that iTunes is installed on your PC.\n\n2. Visit https://good-kidz.com/downloads and download our Good-kidz device configurator to configure child’s device."
        label2.leftInset = 10
        label2.rightInset = 10
        
        label2.adjustsFontSizeToFitWidth = true
        
        
        
        label3.text = "Step 2: Install Mobile Device Management Profile"
        label3.leftInset = 10
        label3.rightInset = 10
        label3.sizeToFit()
        label3.adjustsFontSizeToFitWidth = true
        
        
        label4.text = "1. On your child's device, open Safari and go to https://good-kidz.com/\n\n2. Login using your Good-kidz account.\n\n3. Provide child name and other needed information.\n\n4. Press Pair and install profile.\n\n5. Now you can control your child’s device from your iOS app."
        label4.leftInset = 10
        label4.rightInset = 10
        
        
        regChild.addTarget(self, action: #selector(regChildaccess), for: .touchUpInside)
        regNewChild.addTarget(self, action: #selector(regNewChildaccess), for: .touchUpInside)
        unRegChild.addTarget(self, action: #selector(unRegChildaccess), for: .touchUpInside)
        
        
    }
    
    override func lodingView(temp:Bool){
        // You only need to adjust this frame to move it anywhere you want
        
        if(temp)
        {
            boxView = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50))
            textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        }else
        {
            boxView = UIView(frame: CGRect(x: view.frame.midX - 185, y: view.frame.midY - 25, width: 370, height:50))
            textLabel = UILabel(frame: CGRect(x: 36, y: 0, width: 370, height:50))
        }
        
        boxView.backgroundColor = UIColor.white
        boxView.alpha = 0.8
        boxView.layer.cornerRadius = 10
        
        //Here the spinnier is initialized
        var activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 45, height: 50)
        activityView.startAnimating()
        
        //        textLabel = UILabel(frame: CGRect(x: 36, y: 0, width: 370, height:50))
        textLabel.textColor = UIColor.gray
        //        textLabel.font = UIFont.systemFont(ofSize: 10)
        textLabel.text = "Loading"
        //        textLabel.sizeToFit()
        textLabel.adjustsFontSizeToFitWidth = true
        boxView.addSubview(activityView)
        boxView.addSubview(textLabel)
        self.view.addSubview(boxView)
        
        func lodingView(temp:Bool){
            // You only need to adjust this frame to move it anywhere you want
            
            if(temp)
            {
                boxView = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50))
                textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
            }else
            {
                boxView = UIView(frame: CGRect(x: view.frame.midX - 185, y: view.frame.midY - 25, width: 370, height:50))
                textLabel = UILabel(frame: CGRect(x: 36, y: 0, width: 370, height:50))
            }
            
            boxView.backgroundColor = UIColor.white
            boxView.alpha = 0.8
            boxView.layer.cornerRadius = 10
            
            //Here the spinnier is initialized
            var activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
            activityView.frame = CGRect(x: 0, y: 0, width: 45, height: 50)
            activityView.startAnimating()
            
            //        textLabel = UILabel(frame: CGRect(x: 36, y: 0, width: 370, height:50))
            textLabel.textColor = UIColor.gray
            //        textLabel.font = UIFont.systemFont(ofSize: 10)
            textLabel.text = "Loading"
            //        textLabel.sizeToFit()
            textLabel.adjustsFontSizeToFitWidth = true
            boxView.addSubview(activityView)
            boxView.addSubview(textLabel)
            self.view.addSubview(boxView)
            
        }
    }
    
    
    
    @objc func regChildaccess(){
        
        print("applyaccessButton1 is applied")
        tableView.isHidden = false
        regInstruction.isHidden = true
        self.regInstruction2.isHidden = true
        self.regInstruction3.isHidden = true
        self.regInstruction4.isHidden = true
        tableView1.isHidden = true
        
        label1.isHidden = true
        label2.isHidden = true
        label3.isHidden = true
        label4.isHidden = true
        
    }
    
    @objc func regNewChildaccess(){
        
        
        print("applyaccessButton2 is applied")
        
        tableView.isHidden = true
        regInstruction.isHidden = false
        self.regInstruction2.isHidden = false
        self.regInstruction3.isHidden = false
        self.regInstruction4.isHidden = false
        tableView1.isHidden = true
        
        label1.isHidden = true
        label2.isHidden = true
        label3.isHidden = true
        label4.isHidden = true
        
        
    }
    
    
    @objc func unRegChildaccess(){
        
        print("applyaccessButton2 is applied")
        
        tableView.isHidden = true
        regInstruction.isHidden = true
        self.regInstruction2.isHidden = true
        self.regInstruction3.isHidden = true
        self.regInstruction4.isHidden = true
        tableView1.isHidden = false
        
        
        label1.isHidden = true
        label2.isHidden = true
        label3.isHidden = true
        label4.isHidden = true
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
        //  return UITableViewAutomaticDimension
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print("lllll",childRegisterList.count)
        return childimgeList.count
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChildRegTableViewCell") as! ChildRegTableViewCell
            tableView.layer.cornerRadius = 25.0
            print("llllldddd",self.childRegisterList[indexPath.row])
            print("llllldddd2",self.childimgeList[indexPath.row])
//            DispatchQueue.main.async {
//                if(self.childRegisterList[indexPath.row] != nil){
//                    let profile = self.childimgeList[indexPath.row]
//                    if(profile.isEmpty){
//                        print("nothing")
//                    }else{
//                        let url = URL(string: profile)
//                        UserDefaults.standard.set(url, forKey: "childprofileurl")
//
//                        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
//                        cell.childImg.image = UIImage(data: data!)
//
//                    }
//                }
//            }
            if self.childgenderList[indexPath.row].elementsEqual("Male"){
                cell.childImg.image = #imageLiteral(resourceName: "icon_Little_Boy")
            } else {
                cell.childImg.image = #imageLiteral(resourceName: "icon_little_girl")
            }
            cell.childName.text = self.childRegisterList[indexPath.row]
            DispatchQueue.main.async {
                self.boxView.removeFromSuperview()
            }
            // cell.textLabel?.text = self.childRegisterList[indexPath.row]
            return cell
            
        }
        else if tableView == self.tableView1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChildUnRegTableViewCell") as! ChildUnRegTableViewCell
            
            tableView.layer.cornerRadius = 25.0
            
//            DispatchQueue.main.async {
//                if(self.childRegisterList[indexPath.row] != nil){
//                    let profile = self.childimgeList[indexPath.row]
//                    if(profile.isEmpty){
//                        print("nothing")
//                    }else{
//                        let url = URL(string: profile)
//                        UserDefaults.standard.set(url, forKey: "childprofileurl")
//
//                        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
//                        cell.childImg.image = UIImage(data: data!)
//
//                    }
//                }
//            }
            
            if self.childgenderList[indexPath.row].elementsEqual("Male"){
                cell.childImg.image = #imageLiteral(resourceName: "icon_Little_Boy")
            } else {
                cell.childImg.image = #imageLiteral(resourceName: "icon_little_girl")
            }
            
            DispatchQueue.main.async {
                self.boxView.removeFromSuperview()
            }
            
            cell.childName.text = self.childRegisterList[indexPath.row]
            return cell
            
        }
        
        return UITableViewCell()
    }
    
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if tableView == self.tableView
        {
           
//            print("_________",checkChildisManager(parentID: userID1, childID: childKeyList[indexPath.row]))
            
        self.checkChildisManager(parentID: userID1,childID: childKeyList[indexPath.row]) { (isManaged) -> () in
                if isManaged {
                    print("_________","Yes it is managed...")
                    let selectedItem = self.childRegisterList[indexPath.row]
                    let selecteditemimage = self.childimgeList[indexPath.row]
                    print("You tapped cell number ",selectedItem)
                    let res:ParentManageViewController = self.storyboard?.instantiateViewController(withIdentifier: "ParentManageViewController") as!ParentManageViewController
                    res.strmail = self.childRegisterList[indexPath.row]
                    UserDefaults.standard.set(self.childRegisterList[indexPath.row], forKey: "childname")
                    UserDefaults.standard.set(self.childKeyList[indexPath.row], forKey: "childkey")
                    res.url = self.childimgeList[indexPath.row]
                    UserDefaults.standard.set(self.childimgeList[indexPath.row], forKey: "url")
                    //res.url =
                    self.navigationController?.pushViewController(res, animated: true)
                    print("---parentManageView---ManageChild--- childName",UserDefaults.standard.string(forKey: "childname"))
                }
                else {
                    print("_________","No it is not managed...")
                    let alertController = UIAlertController(title: "Alert", message: "This Child is not fully configured. Please check your child device whether the MDM profile is installed. To install profile go to https://good-kidz.com", preferredStyle: .alert)
                    let defaultAction1 = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction1)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            
            
        }
        else if tableView == self.tableView1 {
            let selectedItem = childRegisterList[indexPath.row]
            UserDefaults.standard.set(childRegisterList[indexPath.row], forKey: "childname")
            
            UserDefaults.standard.set(childKeyList[indexPath.row], forKey: "childkey")
            var childkeyID = UserDefaults.standard.string(forKey: "childkey")!
            
            print(selectedItem ,"is deleted ")
            
            let alertController = UIAlertController(title: "Confirm", message: "Do you want to delete the child?", preferredStyle: .alert)
            let defaultAction1 = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
            alertController.addAction(defaultAction1)
            
            let defaultAction = UIAlertAction(title: "OK", style: .default,  handler: { (action) -> Void in
                let userID1 = Auth.auth().currentUser?.uid
                self.ref.child("Children").child(userID1!).child(childkeyID).removeValue()
                self.ref.child("ChildInfo").child(userID1!).child(childkeyID).removeValue()
            })
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
            
            
        }
        
        
    }
    
    func checkChildisManager(parentID:String, childID:String, completion: @escaping (Bool) -> ())
    {
        ref = Database.database().reference()
        let userID1 = Auth.auth().currentUser?.uid
        
        
        
        ref.child("Children").child(parentID).child(childID).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value

            let value = snapshot.value as? NSDictionary
            print("my value",value)
            if(value!["DeviceInformation"] != nil)
            {
                completion (true)
                
            }else
            {
                completion (false)
            }
            
            
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        //self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
}
class UIMarginLabel: UILabel {
    
    var topInset:       CGFloat = 0
    var rightInset:     CGFloat = 0
    var bottomInset:    CGFloat = 0
    var leftInset:      CGFloat = 0
    
    override func drawText(in rect: CGRect) {
        let insets: UIEdgeInsets = UIEdgeInsets(top: self.topInset, left: self.leftInset, bottom: self.bottomInset, right: self.rightInset)
        self.setNeedsLayout()
        return super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    
    
    //    override func drawText(in rect: CGRect) {
    //        let insets = UIEdgeInsets.init(top: 0, left: 5, bottom: 0, right: 5)
    //        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    //    }
    
}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
