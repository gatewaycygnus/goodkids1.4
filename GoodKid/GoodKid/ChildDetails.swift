//
//  ChildDetails.swift
//  GoodKid
//
//  Created by Kumar Muthusamy on 11/2/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit
import Firebase


class ChildDetails: NSObject {
    var ref: DatabaseReference!
    var name: String = ""
    
    
    init(name: String) {
        
        // Initialize stored properties
        self.name = name
        
    }
    
    func RetrieveFromFirebase() {
        self.ref = Database.database().reference()
        
        let usersRef =  self.ref.child("Children")
        let thisUserRef = Auth.auth().currentUser?.uid
        
      
        
        ref.child("Children").observe(.value, with: {
            snapshot in
            
          
            print("group as Key snapshot---> ",snapshot )
            
            print("group as Key snapshot.children---> ",snapshot.children )
            
            
            var value = (snapshot.value as? NSDictionary)!
            print("my value",value)
            
            for group in snapshot.children {
                
                
                print("group as Key---> ",snapshot.childSnapshot(forPath: (group as AnyObject) as! String).value )
            
            }
                  })
    
    }
    
    
}
