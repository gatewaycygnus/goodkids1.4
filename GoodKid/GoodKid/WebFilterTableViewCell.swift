//
//  WebFilterTableViewCell.swift
//  GoodKid
//
//  Created by Neela's Mac on 26/10/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit

class WebFilterTableViewCell: UITableViewCell {
    
    
   
    @IBOutlet weak var webIcon: UIImageView!
    
    
    @IBOutlet weak var webName: UILabel!
    
    
    @IBOutlet weak var delete: UIButton!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
