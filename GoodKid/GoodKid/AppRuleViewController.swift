//
//  AppRuleViewController.swift
//  GoodKid
//
//  Created by cyg on 10/1/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit
import Firebase
//import CellAnimator
import FirebaseAuth
import XLPagerTabStrip

class AppRuleViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,IndicatorInfoProvider  {
    
    var counter = 0
    var statusList = [String]()
    var appNameList = [String]()
    var appIconList = [String]()
    var bundleidentifier = [String]()
    var statusDictinory:NSDictionary = [String: Any]() as NSDictionary
    var appsdict:NSDictionary = [String: Any]() as NSDictionary
    var statusDict = [String: String]()
    var ref: DatabaseReference!
    var childNumber:String = ""
    var childname = ""
    var task: URLSessionDownloadTask!
    var session: URLSession!
    var cache:NSCache<AnyObject, AnyObject>!
    var snapshot: (Any)? = nil
    var appRuledict:NSDictionary = [String: Any]() as NSDictionary
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subscription: UIButton!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var view2: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        titleLabel.sizeToFit()
        
        // titleLabel.textColor = UIColor.gray
        // titleLabel.font = UIFont.boldSystemFont(ofSize: 30)
        
       // view1.backgroundColor = #colorLiteral(red: 0.9215686275, green: 0.9176470588, blue: 0.9450980392, alpha: 0.9997696995)
   
        
        view2.layer.cornerRadius = 40.0
       view2.layer.masksToBounds = true
    
        session = URLSession.shared
        task = URLSessionDownloadTask()
        self.cache = NSCache()
        
        appNameList.removeAll()
        appIconList.removeAll()
        bundleidentifier.removeAll()
        statusList.removeAll()
      //  statusDict.removeAll()
        
        ref = Database.database().reference()
        let userID1 = Auth.auth().currentUser?.uid
        var childname = UserDefaults.standard.string(forKey: "childkey")
        
        var usersRef = self.ref.child("ChildInfo").child(userID1!)
        
        usersRef.observe(.value, with: { snapshot in
            self.snapshot = snapshot
            
            
            
            if snapshot.hasChild(childname!){
                
                self.appNameList.removeAll()
                self.appIconList.removeAll()
                self.bundleidentifier.removeAll()
                self.statusList.removeAll()
                self.statusDict.removeAll()
                
                
                let value = snapshot.value as? NSDictionary
                print("my value---- ",value)
                if(value?[childname!] != nil)
                {
                    let dict = value?[childname!] as? NSDictionary
                    print("app rule---> ",dict)
                    self.appRuledict = dict!
                    
                    if(self.appRuledict["AppMaster"] != nil)
                    {
                    
                    self.appsdict = self.appRuledict["AppMaster"] as! NSDictionary
                    
                    
                    for name in self.appsdict{
                        var appname = name.key as! String
                        print("appname",name.key)
                        self.appNameList.append(name.key as! String)
                        self.statusDictinory = name.value as! NSDictionary
                        self.statusList.append(self.statusDictinory["Apprule"] as! String)
                        //                DispatchQueue.main.async {
                        self.tableView.reloadData()
                        print("name",self.appNameList.count)
                        //                }
                    }
                    }
                    
                    print("temdict app rule---> ",self.appsdict)
                }
                self.tableView.reloadData()
            } else {
                
                print("---No app rules--- ")
                self.tableView.reloadData()
            }
            
            
        }){ (error) in
            print(error.localizedDescription)
        }
        
        subscription.addTarget(self, action: #selector(applyaccess), for: .touchUpInside)
        subscription.layer.cornerRadius = 15
   
    }
    
    func applyRoundCorner(_ object:AnyObject){
        if #available(iOS 12.0, *) {
            object.layer.cornerRadius = object.frame.size.width / 2
        } else {
            // Fallback on earlier versions
        }
        object.layer.masksToBounds = true
    }
    
    
    @objc func applyaccess(){
        print("Subscription is applied")
        
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubcriptionDetailViewController") as? SubcriptionDetailViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo
    {
        print("----childNumber---",childNumber)
      //  return IndicatorInfo(title: "\(childNumber)")
         return IndicatorInfo(title: "",image:UIImage(named: "Appsfourty"),highlightedImage:UIImage(named: "appfill2"))
        
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appNameList.count
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let block = blockAction(at :indexPath)
        let schedule = scheduleAction(at :indexPath)
        let grant = grantAction(at :indexPath)
        // let delete = deleteAction(at :indexPath)
        
        
        let config = UISwipeActionsConfiguration(actions: [block,schedule,grant])
        
        config.performsFirstActionWithFullSwipe = false
        return config
    
        //return UISwipeActionsConfiguration
        
    }
    
    func blockAction(at indexPath : IndexPath) -> UIContextualAction{
    
        let action = UIContextualAction(style: .normal, title: "BLOCK") { (action, view, completion) in
            
            if(UserDefaults.standard.string(forKey: "subscription") == "free"){
                
                self.upgradeAlert(title: "Upgrade your Subscription", message: "Please upgrade to access this feature")
                
            }
            else{
            
            
            let cell = self.tableView.cellForRow(at: indexPath) as! AppRuleTableViewCell
            cell.shedutingstatebtn.setImage(UIImage(named:"ic_block_on_24dp"), for: UIControlState.normal)
            let userID1 = Auth.auth().currentUser?.uid
            let appname = self.appNameList[indexPath.row]
            print("userid",userID1)
            print("childname",self.childname)
            print("appname",appname)
                
            self.ref.child("ChildInfo").child(userID1!).child(UserDefaults.standard.string(forKey: "childkey")!).child("AppMaster").child(appname).updateChildValues(["Apprule":"B"])
            
            self.statusDict.updateValue("B", forKey: appname)
           self.statusList[indexPath.row] = "B"

            completion(true)
            }
        }
       
        action.backgroundColor = #colorLiteral(red: 0.9137254902, green: 0.06274509804, blue: 0.07843137255, alpha: 1)
        action.image = #imageLiteral(resourceName: "blocki.png")
        
       
        return action
    }
    
    
    func scheduleAction(at indexPath : IndexPath) -> UIContextualAction{
        
        let action = UIContextualAction(style: .normal, title: "SCHEDULE") { (action, view, completion) in
            
            if(UserDefaults.standard.string(forKey: "subscription") == "free"){
                
                self.upgradeAlert(title: "Upgrade your Subscription", message: "Please upgrade to access this feature")
                
            }
            else{
            
            
            
            let cell = self.tableView.cellForRow(at: indexPath) as! AppRuleTableViewCell
            cell.shedutingstatebtn.setImage(UIImage(named:"ic_schedule_on_24dp"), for: UIControlState.normal)
            let userID1 = Auth.auth().currentUser?.uid
            let appname = self.appNameList[indexPath.row]
            print("userid",userID1)
            print("childname",self.childname)
            print("appname",appname)
            self.ref.child("ChildInfo").child(userID1!).child(UserDefaults.standard.string(forKey: "childkey")!).child("AppMaster").child(appname).updateChildValues(["Apprule":"S"])
            self.statusDict.updateValue("S", forKey: appname)
            
            self.statusList[indexPath.row] = "S"

            completion(true)
            }
        }
        action.image = #imageLiteral(resourceName: "clck")
        action.backgroundColor = #colorLiteral(red: 0.2392156863, green: 0.4196078431, blue: 1, alpha: 1)
        return action
    }
    
    
    
    func grantAction(at indexPath : IndexPath) -> UIContextualAction
    {
        let action = UIContextualAction(style: .normal, title: "GRANT") { (action, view, completion) in
            
            if(UserDefaults.standard.string(forKey: "subscription") == "free"){
                
                self.upgradeAlert(title: "Upgrade your Subscription", message: "Please upgrade to access this feature")
                
            }
            else{
            
            
            let cell = self.tableView.cellForRow(at: indexPath) as! AppRuleTableViewCell
            cell.shedutingstatebtn.setImage(UIImage(named:"blckkkk"), for: UIControlState.normal)
            let userID1 = Auth.auth().currentUser?.uid
            let appname = self.appNameList[indexPath.row]
                
                
            print("userid",userID1)
            print("childname",self.childname)
            print("appname",appname)
            self.ref.child("ChildInfo").child(userID1!).child(UserDefaults.standard.string(forKey: "childkey")!).child("AppMaster").child(appname).updateChildValues(["Apprule":"A"])
                
            self.statusDict.updateValue("A", forKey: appname)
            self.statusList[indexPath.row] = "A"
//
            completion(true)
            }
        }
       
        action.image = #imageLiteral(resourceName: "granti.png")
        action.backgroundColor = #colorLiteral(red: 0.1254901961, green: 0.5764705882, blue: 0.137254902, alpha: 1)
        return action
    }
    
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell = tableView.dequeueReusableCell(withIdentifier: "AppRuleTableViewCell") as! AppRuleTableViewCell
        
        let appdetaildict = appsdict[appNameList[indexPath.row]] as! NSDictionary
        let appicon = appdetaildict["AppIcon"] as? String
        let status = appdetaildict["Apprule"] as? String
        let Bundleidentifier = appdetaildict["BundleIdentifier"] as? String
        print("bundleidentifier",Bundleidentifier)
        
              print("statusDict",self.statusDict)
        print("ststuslist",self.statusList)
       // self.statusList.append(status as! String)
//
        
        
        tableView.layer.cornerRadius = 30.0
        // for up corner
        tableView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        cell.appName.text = self.appNameList[indexPath.row]
//        cell.appIcon.image = UIImage(data: data!)
        
        
        if (self.cache.object(forKey: (indexPath as NSIndexPath).row as AnyObject) != nil){
            // 2
            // Use cache
            print("Cached image used, no need to download it")
            cell.appIcon.image = self.cache.object(forKey: (indexPath as NSIndexPath).row as AnyObject) as? UIImage
        }else{
            // 3
//            let artworkUrl = dictionary["artworkUrl100"] as! String
            let url:URL! = URL(string: appicon!)
            if url != nil{
            task = session.downloadTask(with: url, completionHandler: { (location, response, error) -> Void in
                if let data = try? Data(contentsOf: url){
                    // 4
                    DispatchQueue.main.async(execute: { () -> Void in
                        // 5
                        // Before we assign the image, check whether the current cell is visible
                        if let updateCell = tableView.cellForRow(at: indexPath) {
                            let img:UIImage! = UIImage(data: data)
                            cell.appIcon.image = img
                            self.cache.setObject(img, forKey: (indexPath as NSIndexPath).row as AnyObject)
                        }
                    })
                }
            })
            task.resume()
        }
        }
      
        if(statusList[indexPath.row] == "S"){
            cell.shedutingstatebtn.setImage(UIImage(named:"ic_schedule_on_24dp"), for: UIControlState.normal)
        }
        if(statusList[indexPath.row] ==  "A"){
            cell.shedutingstatebtn.setImage(UIImage(named:"blckkkk"), for: UIControlState.normal)
            
        }
       if(statusList[indexPath.row] == "B"){
            cell.shedutingstatebtn.setImage(UIImage(named:"ic_block_on_24dp"), for: UIControlState.normal)
            
        }
        
        
        
        if(self.appNameList[indexPath.row] == "Instagram") || (self.appNameList[indexPath.row] == "WhatsApp"){
            
            // cell.blockimage.setImage(UIImage(named: "ic_schedule_on_24dp"), for: .normal)
            
            cell.appName.sizeToFit()
            //  cell.appName.adjustsFontSizeToFitWidth = true
            
            print("---tableview---- ",self.appNameList[indexPath.row])
            return cell
            
        }
        //  cell.sheduleimage.setImage(UIImage(named: "ic_block_on_24dp"), for: .normal)
        
        cell.appName.sizeToFit()
        //  cell.appName.adjustsFontSizeToFitWidth = true
        
        
        print("---tableview---- ",self.appNameList[indexPath.row])
        return cell
    }
    
    func upgradeAlert(title:String,message:String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let defaultAction1 = UIAlertAction(title: "CANCEL", style: .cancel, handler: nil)
        alertController.addAction(defaultAction1)
        
        let defaultAction = UIAlertAction(title: "UPGRADE", style: .default,  handler: { (action) -> Void in
            
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            
            
        })
        
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
        
        
        
    }
    
    // Button Action
    
    @objc func btnAction(_ sender: AnyObject) {
        
        print("alwaysBlock is applied")
        var position: CGPoint = sender.convert(.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: position)
        let cell: UITableViewCell = tableView.cellForRow(at: indexPath!)! as
        UITableViewCell
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = appNameList[indexPath.row]
        print("You tapped cell number ",selectedItem)
        let cell = tableView.cellForRow(at: indexPath) as! AppRuleTableViewCell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        
        print("ADFSFFF",statusDict.count)
        if (statusDict.count == 0){

            let alertController = UIAlertController(title: "Alert", message: "No changes to save.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        else
        {
        for dict in statusDict {
            let userID1 = Auth.auth().currentUser?.uid
    self.ref.child("ChildInfo").child(userID1!).child(UserDefaults.standard.string(forKey: "childkey")!).child("AppMaster").child(dict.key).updateChildValues(["Apprule":dict.value])
            
        }
        statusDict.removeAll()
            let alertController = UIAlertController(title: "Alert", message: "Synched Successfully.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
}
