//
//  WebFilterViewController.swift
//  GoodKid
//
//  Created by Kumar Muthusamy on 10/25/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Firebase
import FirebaseDatabase
class WebFilterViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,IndicatorInfoProvider  {
    
    @IBOutlet weak var websiteName: UITextField!
    
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var tabview: UIView!
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var webFilterList: [String] = []
    
    
    
    @IBOutlet weak var add: UIButton!
    
    
    @IBOutlet weak var disable: UIButton!
    
    var disableFlag:Bool = false
 
    var childNumber:String = ""
    var usersRef:String = ""
    var currentUser:String = ""
    var childkey:String = ""
    
    
    var ref: DatabaseReference!
    var snapshot: (Any)? = nil
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        tableView.layer.cornerRadius = 40.0
        
        // for down corner
        tableView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
       websiteName.delegate = self
        view2.layer.cornerRadius = 40.0
        // for up corner
        view2.layer.maskedCorners  = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        // Do any additional setup after loading the view.
        
        print("webFilterList.count--- ",webFilterList.count)
        
        add.addTarget(self, action: #selector(AddAction), for: .touchUpInside)
        disable.addTarget(self, action: #selector(DisableAction), for: .touchUpInside)
        
        add.layer.borderColor = UIColor.gray.cgColor
        add.layer.borderWidth = 2.0
        
        disable.layer.borderColor = UIColor.gray.cgColor
        disable.layer.borderWidth = 2.0
        
        currentUser = (Auth.auth().currentUser?.uid)!
        childkey = UserDefaults.standard.string(forKey: "childkey")!
        
        print("---search button clicked-----thisUserRef ",currentUser,childkey)
        
        
        websiteName.layer.borderWidth = 2.0
        websiteName.layer.borderColor = UIColor.gray.cgColor
        
        
        self.ref = Database.database().reference()
        
        
        var usersRef = self.ref.child("ChildInfo").child(currentUser).child(childkey).child("WebFilter")
        
        usersRef.observe(.value, with: { snapshot in
            self.snapshot = snapshot
            
            print("---snapshot webfilter ",snapshot.key)
            print("---snapshot webfilter ",snapshot.value)
            
            print("---snapshot webfilter ",snapshot.hasChild("blackListedURLs"))
            
            
            if snapshot.hasChild("blackListedURLs"){
                
                self.webFilterList = snapshot.childSnapshot(forPath: "blackListedURLs").value as! [String]
                print("---snapshot webfilter123456 ",    self.webFilterList)
                
                self.tableView.isHidden = false
            } else {
                self.tableView.isHidden = true
                print("---No WebFilters--- ")
            }
            
            
            self.tableView.reloadData()
            
            
        })
        
        tableView.reloadData()
    }
    
    
    
    func textFieldShouldReturn(_ textfield: UITextField) -> Bool {
        
        websiteName.resignFirstResponder()
        return true;
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo
    {
        print("----childNumber---",childNumber)
    //    return IndicatorInfo(title: "\(childNumber)
        
        
        
        
        return IndicatorInfo(title: "",image:UIImage(named: "webfil"),highlightedImage:UIImage(named: "webfil1"))
    
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("webFilterList.count---2222 ",webFilterList.count)
        
      
        return webFilterList.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell = tableView.dequeueReusableCell(withIdentifier: "WebFilterTableViewCell") as! WebFilterTableViewCell
        

        cell.webName.text = self.webFilterList[indexPath.row]
        
        
        cell.delete.addTarget(self, action: #selector(DeleteAction), for: .touchUpInside)
        
        print("---tableview---- ",self.webFilterList[indexPath.row])
        return cell
    }

    // AddAction Action
    
    @objc func AddAction(_ sender: AnyObject) {
        
        
        let name = websiteName.text
        
        print("name-----websiteName.text> ",websiteName.text?.isEmpty)
        
        if !webFilterList.contains(name as! String) && websiteName.text?.isEmpty == false{
            webFilterList.append(name as! String)
            print("name-----> ",name)
            
            print("name-----111 websiteName.text> ",websiteName.text?.isEmpty)
            
            self.ref = Database.database().reference()
            
            var usersRef = self.ref.child("ChildInfo")
            usersRef.child(currentUser).child(childkey).child("WebFilter").child("blackListedURLs").setValue(webFilterList)
            tableView.reloadData()
        } else {
            
            
            print("name-----222 websiteName.text> ",websiteName.text?.isEmpty)
            
              websiteName.attributedPlaceholder = NSAttributedString(string:"Enter the site,eg.:allowed-site.com", attributes:[NSAttributedStringKey.foregroundColor: UIColor.red,NSAttributedStringKey.font :UIFont.systemFont(ofSize: 15)])
            
        }
        
        if webFilterList.count > 0 {
            tableView.isHidden = false
            websiteName.text = ""
        } else {
            tableView.isHidden = true
        }
        
        tableView.reloadData()
    
        
    }
    

    // Disable Action
    
    @objc func DisableAction(_ sender: AnyObject) {
        
        tableView.reloadData()
        
    }
// alwaysBlock Action
    
    @objc func DeleteAction(_ sender: AnyObject) {
        
        print("Delete is applied")
        
        var position: CGPoint = sender.convert(.zero, to: self.tableView)
        
        let indexPath = self.tableView.indexPathForRow(at: position)
        let cell: UITableViewCell = tableView.cellForRow(at: indexPath!)! as
        UITableViewCell
        
        if let tableView = tableView {
            let point = tableView.convert(sender.center, from: sender.superview!)
            
            if let wantedIndexPath = self.tableView.indexPathForRow(at: point)
            {
                
                let cell = tableView.cellForRow(at: indexPath!) as! WebFilterTableViewCell
                
                let temp = webFilterList[(wantedIndexPath.row)]
                
                print("---temp--- ",temp)
                if webFilterList.contains(temp)  {
                    print("---scheduleAppList---contains ",temp)
                    let position = webFilterList.index(of: temp)
                    webFilterList.remove(at: position!)
                }
                
                tableView.reloadData()
                print("---scheduleAppList---5555 ",webFilterList)
                var usersRef =  self.ref.child("ChildInfo")
            usersRef.child(currentUser).child(childkey).child("WebFilter").child("blackListedURLs").setValue(webFilterList)
        
            }
        }
        
    }
    

    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = webFilterList[indexPath.row]
        print("You tapped cell number ",selectedItem)
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
