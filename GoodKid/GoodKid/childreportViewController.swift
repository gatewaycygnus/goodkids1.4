//
//  childreportViewController.swift
//  GoodKid
//
//  Created by Kumar Muthusamy on 10/3/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import UIKit
import CellAnimator

class childreportViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
 let dropdown : NSMutableArray = ["Feedback","other"]
    @IBOutlet weak var tableviewdropdwn: UITableView!
    @IBOutlet weak var applistView: UIView!
    @IBOutlet weak var Dropdownbtn: UIButton!
var counter = 0
    ///@IBOutlet weak var labelname: UILabel!
    
    let Appname = [("Facebook"),("Whatsapp"),("Instagram"),("Youtube"),("Newshunt")]
    let AppImages = [UIImage(named: "fb"), UIImage(named: "whtat"), UIImage(named: "insta"), UIImage(named: "insta"), UIImage(named: "insta")]
    
    
    @IBOutlet weak var applisttableview: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applistView.layer.borderWidth = 2
        
        applistView.layer.borderColor = UIColor.black.cgColor
tableviewdropdwn.isHidden = true
        Dropdownbtn.layer.cornerRadius = 20
        Dropdownbtn.layer.borderWidth = 1
        Dropdownbtn.layer.borderColor = UIColor.black.cgColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func Dropdownbtn(_ sender: Any) {
        
        if tableviewdropdwn.isHidden == true{
            tableviewdropdwn.isHidden = false
            
        }else{
            tableviewdropdwn.isHidden = true
        }
        
        
        
        
        
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == applisttableview{
            return Appname.count
        }
        
        else{
        return dropdown.count
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        
      
        
        
        
        let delete = UIContextualAction(style: .normal, title: "Block") { (action, view,nil) in
            
            print("hhhsh")
            
        }
        delete.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        delete.image = #imageLiteral(resourceName: "clck")
        let shedule = UIContextualAction(style: .normal, title: "Schedule") { (action, view,nil) in

            print("hhhsh")

        }
        shedule.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        shedule.image = #imageLiteral(resourceName: "blckk")

        let grant = UIContextualAction(style: .normal, title: "Grant") { (action, view,nil) in
            let cell = tableView.cellForRow(at: indexPath) as! childreportTableViewCell
            
            CellAnimator.animateCell(cell: cell, withTransform: CellAnimator.TransformTilt, andDuration: 1)
            
            print("hhhsh")
            self.counter = 1
            
        }
        print("counter",counter)
        if counter == 1{
            print("gggggg")
            grant.backgroundColor = #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)
            grant.image = #imageLiteral(resourceName: "clck")
        }
        else{
            print("hffffbf")
        grant.backgroundColor = #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1)
        grant.image = #imageLiteral(resourceName: "clck")
        }
        
        
//        let grant = UIContextualAction(style: .normal, title: "Grant") { (action, view,nil) in
//
//            print("hhhsh")
//
//        }
        //shedule.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        //shedule.image = #imageLiteral(resourceName: "ic_check_circle_on_24dp")
        
        
        
        
        
        let cofig = UISwipeActionsConfiguration(actions: [delete,shedule,grant])
        cofig.performsFirstActionWithFullSwipe = false
        return cofig
        
        
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let more = UITableViewRowAction(style: .normal, title: "More", handler: { action , indexPath in
            
            
            print("More Click SuccessFully")
        })
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete", handler: { action , indexPath in
            print("Delete Click SuccessFully")
        })
        
        return [more,delete]
    } 
    
    
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == applisttableview{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! childreportTableViewCell
            cell.image1.image=self.AppImages[indexPath .row]
            cell.appname.text=self.Appname[indexPath .row]
            cell.timelabel.text = self.Appname[indexPath .row]
            return cell
            
        }
        
        else{
        
        // create a new cell if needed or reuse an old one
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell!
        
        // set the text from the data model
        cell.textLabel?.text = self.dropdown[indexPath.row] as! String
        
        return cell
        }
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView != applisttableview{
        let selectedItem = dropdown[indexPath.row]
        Dropdownbtn.setTitle(selectedItem as! String, for: UIControlState.normal)
        print("You tapped cell number \(indexPath.row).")
        tableView.isHidden = true
        }
    }
    

}
