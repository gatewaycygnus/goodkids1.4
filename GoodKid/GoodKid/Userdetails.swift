//
//  Userdetails.swift
//  GoodKid
//
//  Created by Kumar Muthusamy on 9/17/18.
//  Copyright © 2018 Kumar Muthusamy. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import Foundation

class Userdetails: NSObject {
     var ref: DatabaseReference!
    var name: String
    var email: String
    var password: String
    var mobilenumber: String
    var signupdate: String
    var numberofdevices: String
    var noofBlocks: Int
    var Subscripition: String
    var profileurl : String
    var noofgrants : Int
    
    init(name: String, email: String, password: String, mobilenumber: String, signupdate: String,numberofdevices: String, noofBlocks: Int, Subscripition:  String,profileurl : String,noofgrants : Int) {
        
        // Initialize stored properties
        self.name = name
        self.email = email
        self.password = password
        self.mobilenumber = mobilenumber
        self.signupdate = signupdate
         self.numberofdevices = numberofdevices
         self.noofBlocks = noofBlocks
         self.Subscripition = Subscripition
        self.noofgrants = noofgrants
        self.profileurl = profileurl
        
        
    }
    //saving data into firebase
    

    func saveToFirebase() {
        self.ref = Database.database().reference()


        let usersRef =  self.ref.child("Users")
       // let thisUserRef = usersRef.childByAutoId()
        
        
      let thisUserRef = Auth.auth().currentUser?.uid
        
        //print("id value",thisUserRef.key)
        let dict = ["name": self.name, "email": self.email, "password": self.password, "mobilenumber": self.mobilenumber,"no of device": self.numberofdevices,"no of blocks": self.noofBlocks,"subscription": self.Subscripition,"signupdate":self.signupdate,"profileurl": self.profileurl,"noofgrants": self.noofgrants] as [String : Any]
//
        
        usersRef.child(thisUserRef!).setValue(dict)
    }
}
